# FastWeb

快速加载webview框架，更轻便，更快捷

> 设计原理：
    基于Android原生的Webview构建的一个高效加载，使用方便且功能强大的库。
    采用Build设计模式，提供更简洁的调用方式，并且提供了一系列的api供开发者
    调用。开发者可以更快更高效的进行混合原生开发。
    
> 使用介绍:
    
    · 加载百度网页为例 
    
    `
    mFastWeb = FastWeb.with(this)
                .setFastWebParent(container,new LinearLayout.LayoutParams(-1,-1))
                .useDefaultIndicator()
                .setSecurityType(SecurityType.STRICT)
                .setOpenOtherPageWays(OpenOtherPageWays.ASK)
                .interceptUnkownUrl()
                .createFastWeb()
                .ready()
                .go("https://www.baidu.com");
    `
    
### 后续将会持续更新，不断完善......