package com.artcool.fastweb.core.creator;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.webkit.WebView;
import android.widget.FrameLayout;

import com.artcool.fastweb.core.R;
import com.artcool.fastweb.core.base.BaseIndicatorView;
import com.artcool.fastweb.core.base.listener.BaseIndicatorSpecListener;
import com.artcool.fastweb.core.config.FastWebConfig;
import com.artcool.fastweb.core.indicator.WebIndicator;
import com.artcool.fastweb.core.layout.WebParentLayout;
import com.artcool.fastweb.core.layout.listener.WebLayoutListener;
import com.artcool.fastweb.core.utils.FastWebUtils;
import com.artcool.fastweb.core.view.FastWebView;

/**
 * @author wuyibin
 * @date 2019/5/14
 */
public class DefaultWebViewCreator implements WebViewCreator {
    private Activity mActivity;
    private ViewGroup mViewGroup;
    private boolean isDefaultProgress;
    private int index;
    private BaseIndicatorView mProgressView;
    private ViewGroup.LayoutParams mLayoutParams;
    private int mColor = -1;
    private int mHeight;
    private boolean isCreated = false;
    private WebLayoutListener mWebListener;
    private BaseIndicatorSpecListener mBaseListener = null;
    private WebView mWebView;
    private FrameLayout mFrameLayout = null;
    private View mTargetProgress;

    /**
     * 使用默认的进度条
     *
     * @param activity
     * @param viewGroup
     * @param params
     * @param index
     * @param color
     * @param height
     * @param webView
     * @param listener
     */
    public DefaultWebViewCreator(@NonNull Activity activity,
                                 @Nullable ViewGroup viewGroup,
                                 ViewGroup.LayoutParams params,
                                 int index,
                                 int color,
                                 int height,
                                 WebView webView,
                                 WebLayoutListener listener) {
        this.mActivity = activity;
        this.mViewGroup = viewGroup;
        this.isDefaultProgress = true;
        this.index = index;
        this.mLayoutParams = params;
        this.mColor = color;
        this.mHeight = height;
        this.mWebView = webView;
        this.mWebListener = listener;
    }

    /**
     * 关闭进度条
     *
     * @param activity
     * @param viewGroup
     * @param params
     * @param index
     * @param webView
     * @param listener
     */
    public DefaultWebViewCreator(@NonNull Activity activity,
                                 @Nullable ViewGroup viewGroup,
                                 ViewGroup.LayoutParams params,
                                 int index,
                                 WebView webView,
                                 WebLayoutListener listener) {
        this.mActivity = activity;
        this.mViewGroup = viewGroup;
        this.isDefaultProgress = false;
        this.index = index;
        this.mLayoutParams = params;
        this.mWebView = webView;
        this.mWebListener = listener;
    }

    /**
     * 自定义进度条
     *
     * @param activity
     * @param viewGroup
     * @param params
     * @param index
     * @param progress
     * @param webView
     * @param listener
     */
    public DefaultWebViewCreator(@NonNull Activity activity,
                                 @Nullable ViewGroup viewGroup,
                                 ViewGroup.LayoutParams params,
                                 int index,
                                 BaseIndicatorView progress,
                                 WebView webView,
                                 WebLayoutListener listener) {
        this.mActivity = activity;
        this.mViewGroup = viewGroup;
        this.isDefaultProgress = false;
        this.index = index;
        this.mProgressView = progress;
        this.mLayoutParams = params;
        this.mWebView = webView;
        this.mWebListener = listener;
    }

    public FrameLayout getFrameLayout() {
        return mFrameLayout;
    }

    public View getTargetProgress() {
        return mTargetProgress;
    }

    public void setTargetProgress(View targetProgress) {
        this.mTargetProgress = targetProgress;
    }

    @Override
    public DefaultWebViewCreator create() {
        if (isCreated) {
            return this;
        }
        isCreated = true;
        ViewGroup viewGroup = this.mViewGroup;
        if (viewGroup == null) {
            viewGroup = this.mFrameLayout = (FrameLayout) createLayout();
            mActivity.setContentView(viewGroup);
        } else {
            if (index == -1) {
                viewGroup.addView(this.mFrameLayout = (FrameLayout) createLayout(),mLayoutParams);
            } else {
                viewGroup.addView(this.mFrameLayout = (FrameLayout) createLayout(),index,mLayoutParams);
            }
        }
        return this;
    }

    private ViewGroup createLayout() {
        Activity activity = this.mActivity;
        WebParentLayout mFrameLayout = new WebParentLayout(activity);
        mFrameLayout.setId(R.id.web_parent_layout_id);
        mFrameLayout.setBackgroundColor(Color.WHITE);
        View target = mWebListener == null ? (this.mWebView = createWebView()) : webLayout();
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        mFrameLayout.addView(target, layoutParams);
        mFrameLayout.bindWebView(this.mWebView);
        if (mWebView instanceof FastWebView) {
            FastWebConfig.WEBVIEW_TYPE = FastWebConfig.WEBVIEW_FASTWEB_SAFE_TYPE;
        }
        ViewStub viewStub = new ViewStub(mActivity);
        viewStub.setId(R.id.mainframe_error_viewsub_id);
        mFrameLayout.addView(viewStub, new FrameLayout.LayoutParams(-1, -1));
        if (isDefaultProgress) {
            FrameLayout.LayoutParams params;
            WebIndicator indicator = new WebIndicator(mActivity);
            if (mHeight > 0) {
                params = new FrameLayout.LayoutParams(-2, FastWebUtils.dp2px(mActivity, mHeight));
            } else {
                params = indicator.offerLayoutParams();
            }
            if (mColor != -1) {
                indicator.setColor(mColor);
            }
            params.gravity = Gravity.TOP;
            mFrameLayout.addView((View) (this.mBaseListener = indicator), params);
            indicator.setVisibility(View.GONE);
        } else if (!isDefaultProgress && mProgressView != null) {
            mFrameLayout.addView((View) (this.mBaseListener = mProgressView),mProgressView.offerLayoutParams());
            mProgressView.setVisibility(View.GONE);
        }
        return mFrameLayout;
    }

    private View webLayout() {
        WebView webView;
        if ((webView = mWebListener.getWebView()) == null) {
            webView = createWebView();
            mWebListener.getLayout().addView(webView, -1, -1);
        } else {
            FastWebConfig.WEBVIEW_TYPE = FastWebConfig.WEBVIEW_CUSTOM_TYPE;
        }
        this.mWebView = webView;
        return mWebListener.getLayout();
    }

    private WebView createWebView() {
        WebView webView;
        if (this.mWebView != null) {
            webView = this.mWebView;
            FastWebConfig.WEBVIEW_TYPE = FastWebConfig.WEBVIEW_CUSTOM_TYPE;
        } else if (FastWebConfig.IS_KITKAT_OR_BELOW_KITKAT) {
            webView = new FastWebView(mActivity);
            FastWebConfig.WEBVIEW_TYPE = FastWebConfig.WEBVIEW_FASTWEB_SAFE_TYPE;
        } else {
            webView = new WebView(mActivity);
            FastWebConfig.WEBVIEW_TYPE = FastWebConfig.WEBVIEW_DEFAULT_TYPE;
        }
        return webView;
    }

    @Override
    public WebView getWebView() {
        return mWebView;
    }

    public void setWebView(WebView webView) {
        this.mWebView = webView;
    }

    @Override
    public FrameLayout getWebParenLayout() {
        return mFrameLayout;
    }

    @Override
    public BaseIndicatorSpecListener offer() {
        return mBaseListener;
    }
}
