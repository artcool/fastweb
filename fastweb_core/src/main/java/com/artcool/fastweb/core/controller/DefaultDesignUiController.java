package com.artcool.fastweb.core.controller;

import android.app.Activity;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebView;
import android.widget.TextView;

import com.artcool.fastweb.core.R;
import com.artcool.fastweb.core.layout.WebParentLayout;
import com.artcool.fastweb.core.utils.FastWebUtils;

/**
 *
 * @author wuyibin
 * @date 2019/5/5
 */
public class DefaultDesignUiController extends DefaultUiController {

    private BottomSheetDialog mBottomSheetDialog;
    private static final int RECYCLEVIEW_ID = 0x1001;
    private Activity mActivity = null;
    private WebParentLayout mWebParentLayout;
    private LayoutInflater mLayoutInflater;

    @Override
    public void onJsAlert(WebView webView, String url, String message) {
        onJsAlertInternal(webView,message);
    }

    private void onJsAlertInternal(WebView webView, String message) {
        Activity mActivity = this.mActivity;
        if (mActivity == null || mActivity.isFinishing()) {
            return;
        }
        try {
            FastWebUtils.show(webView,
                    message,
                    Snackbar.LENGTH_SHORT,
                    Color.WHITE,
                    mActivity.getResources().getColor(R.color.black),
                    null,
                    -1,
                    null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onJsConfirm(WebView webView, String url, String message, JsResult result) {
        super.onJsConfirm(webView, url, message, result);
    }

    @Override
    public void onSelectItemsPrompt(WebView webView, String url, String[] ways, Handler.Callback callback) {
        showSelectInternal(webView,url,ways,callback);
    }

    @Override
    public void onForceDownloadAlert(String url, Handler.Callback callback) {
        super.onForceDownloadAlert(url, callback);
    }

    @Override
    public void onJsPrompt(WebView webView, String url, String message, String defaultValue, JsPromptResult result) {
        super.onJsPrompt(webView, url, message, defaultValue, result);
    }

    @Override
    protected void bindSupportWebParent(WebParentLayout webParentLayout, Activity activity) {
        super.bindSupportWebParent(webParentLayout, activity);
        this.mActivity = activity;
        this.mWebParentLayout = webParentLayout;
        mLayoutInflater = LayoutInflater.from(mActivity);
    }

    @Override
    public void onShowMessage(String message, String intent) {
        if (!TextUtils.isEmpty(intent) && intent.contains("performDownload")) {
            return;
        }
        onJsAlertInternal(mWebParentLayout.getWebView(),message);
    }

    private void showSelectInternal(WebView webView, String url, String[] ways, Handler.Callback callback) {
        RecyclerView recyclerView;
        if (mBottomSheetDialog == null) {
            mBottomSheetDialog = new BottomSheetDialog(mActivity);
            recyclerView = new RecyclerView(mActivity);
            recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
            recyclerView.setId(RECYCLEVIEW_ID);
            mBottomSheetDialog.setContentView(recyclerView);
        }
        recyclerView = mBottomSheetDialog.getDelegate().findViewById(RECYCLEVIEW_ID);
        recyclerView.setAdapter(getAdapter(ways,callback));
    }

    private RecyclerView.Adapter getAdapter(final String[] ways, final Handler.Callback callback) {
        return new RecyclerView.Adapter<BottomSheetHolder>() {
            @NonNull
            @Override
            public BottomSheetHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                return new BottomSheetHolder(mLayoutInflater.inflate(android.R.layout.simple_list_item_1,viewGroup,false));
            }

            @Override
            public void onBindViewHolder(@NonNull BottomSheetHolder holder, final int i) {
                TypedValue value = new TypedValue();
                mActivity.getTheme().resolveAttribute(android.R.attr.selectableItemBackground,value,true);
                holder.mTextView.setBackgroundResource(value.resourceId);
                holder.mTextView.setText(ways[i]);
                holder.mTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mBottomSheetDialog != null && mBottomSheetDialog.isShowing()) {
                            mBottomSheetDialog.dismiss();
                        }
                        Message mMessage = Message.obtain();
                        mMessage.what = i;
                        callback.handleMessage(mMessage);
                    }
                });
            }

            @Override
            public int getItemCount() {
                return ways.length;
            }
        };
    }

    private static class BottomSheetHolder extends RecyclerView.ViewHolder{
        TextView mTextView;
        public BottomSheetHolder(@NonNull View itemView) {
            super(itemView);
            mTextView = itemView.findViewById(android.R.id.text1);
        }
    }
}
