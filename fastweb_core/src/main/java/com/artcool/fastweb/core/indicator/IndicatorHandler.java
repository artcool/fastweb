package com.artcool.fastweb.core.indicator;

import android.webkit.WebView;

import com.artcool.fastweb.core.base.listener.BaseIndicatorSpecListener;
import com.artcool.fastweb.core.controller.IndicatorController;

/**
 * @author wuyibin
 * @date 2019/5/23
 */
public class IndicatorHandler implements IndicatorController {
    private BaseIndicatorSpecListener mListener;

    public static IndicatorHandler getInstance() {
        return new IndicatorHandler();
    }

    public IndicatorHandler injectIndicator(BaseIndicatorSpecListener listener) {
        this.mListener = listener;
        return this;
    }

    @Override
    public void progress(WebView webView, int newProgress) {
        if (newProgress == 0) {
            reset();
        } else if (newProgress > 0 && newProgress <= 10) {
            showIndicator();
        } else if (newProgress > 10 && newProgress < 95) {
            setProgress(newProgress);
        } else {
            setProgress(newProgress);
            finish();
        }
    }

    private void reset() {
        if (mListener != null) {
            mListener.reset();
        }
    }

    @Override
    public BaseIndicatorSpecListener offerIndicator() {
        return mListener;
    }

    @Override
    public void showIndicator() {
        if (mListener != null) {
            mListener.show();
        }
    }

    @Override
    public void setProgress(int newProgress) {
        if (mListener != null) {
            mListener.setProgress(newProgress);
        }
    }

    @Override
    public void finish() {
        if (mListener != null) {
            mListener.hide();
        }
    }
}
