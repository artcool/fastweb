package com.artcool.fastweb.core.js.impl;

import android.webkit.WebView;

import com.artcool.fastweb.core.js.BaseJsInterfaceHolder;
import com.artcool.fastweb.core.js.JsInterfaceHolder;
import com.artcool.fastweb.core.security.SecurityType;

import java.util.Map;
import java.util.Set;

/**
 *
 * @author wuyibin
 * @date 2019/5/22
 */
public class JsInterfaceHolderImpl extends BaseJsInterfaceHolder {
    private WebView mWebView;
    private SecurityType mSecurityType;

    JsInterfaceHolderImpl(WebView webView,SecurityType type) {
        super(type);
        this.mWebView = webView;
        this.mSecurityType = type;
    }

    public static JsInterfaceHolderImpl getJsInterfaceHolder(WebView webView,SecurityType type) {
        return new JsInterfaceHolderImpl(webView, type);
    }



    @Override
    public JsInterfaceHolder addJavaObjects(Map<String, Object> maps) {
        if (!checkScurity()) {
            return this;
        }
        Set<Map.Entry<String,Object>> set = maps.entrySet();
        for (Map.Entry<String,Object> entry : set) {
            Object value = entry.getValue();
            boolean b = checkObject(value);
            if (!b) {
                throw new RuntimeException("该对象没有提供js方法调用，请检查！");
            } else {
                addJavaObjectDirect(entry.getKey(),value);
            }
        }
        return this;
    }

    private JsInterfaceHolder addJavaObjectDirect(String key, Object value) {
        mWebView.addJavascriptInterface(value,key);
        return this;
    }

    @Override
    public JsInterfaceHolder addJavaObject(String key, Object value) {
        if (!checkScurity()) {
            return this;
        }
        boolean b = checkObject(value);
        if (!b) {
            throw new RuntimeException("该对象没有提供js方法调用，请检查！");
        } else {
            addJavaObjectDirect(key, value);
        }
        return this;
    }
}
