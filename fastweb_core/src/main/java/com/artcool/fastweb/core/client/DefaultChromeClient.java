package com.artcool.fastweb.core.client;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;

import com.artcool.fastweb.core.base.BaseMiddleWebChrome;
import com.artcool.fastweb.core.controller.AbsFastWebUIController;
import com.artcool.fastweb.core.controller.IndicatorController;
import com.artcool.fastweb.core.interceptor.PermissionInterceptor;
import com.artcool.fastweb.core.permissions.Action;
import com.artcool.fastweb.core.permissions.ActionActivity;
import com.artcool.fastweb.core.permissions.FastWebPermissions;
import com.artcool.fastweb.core.utils.FastWebUtils;
import com.artcool.fastweb.core.video.VideoListener;

import java.lang.ref.WeakReference;
import java.util.List;

import static com.artcool.fastweb.core.permissions.ActionActivity.KEY_FROM_INTENTION;

/**
 * @author wuyibin
 * @date 2019/5/23
 */
public class DefaultChromeClient extends BaseMiddleWebChrome {
    /**
     * 用于反射，用户是否重写来该方法
     */
    public static final String ANDROID_WEBCHROMECLIENT_PATH = "android.webkit.WebChromeClient";
    public static final int FROM_CODE_INTENTION = 0x18;
    public static final int FROM_CODE_INTENTION_LOCATION = FROM_CODE_INTENTION << 2;
    private WeakReference<Activity> mActivityWeakReference;
    private WebChromeClient mWebChromeClient;
    private boolean isWrapper;
    private VideoListener mVideoListener;
    private PermissionInterceptor mPermissionInterceptor;
    private WebView mWebView;
    /**
     * Web端触发的定位 mOrigin
     */
    private String mOrigin = null;
    /**
     * Web 端触发的定位 Callback 回调成功，或者失败
     */
    private GeolocationPermissions.Callback mCallBack = null;
    private WeakReference<AbsFastWebUIController> mWebUIController;
    /**
     * 进度条控制器
     */
    private IndicatorController mIndicatorController;
    private ActionActivity.PermissionListener mPermissionListener = new ActionActivity.PermissionListener() {
        @Override
        public void onRequestPermissionsResult(@NonNull String[] permissions, @NonNull int[] grantResult, Bundle extras) {
            if (extras.getInt(KEY_FROM_INTENTION) == FROM_CODE_INTENTION_LOCATION) {
                boolean hasPermission = FastWebUtils.hasPermission(mActivityWeakReference.get(), permissions);
                if (mCallBack != null) {
                    if (hasPermission) {
                        mCallBack.invoke(mOrigin, true, false);
                    } else {
                        mCallBack.invoke(mOrigin, false, false);
                    }
                    mCallBack = null;
                    mOrigin = null;
                }
                if (!hasPermission && mWebUIController.get() != null) {
                    mWebUIController.get().onPermissionDeny(FastWebPermissions.LOCATION, FastWebPermissions.ACTION_LOCATION, "Location");
                }
            }
        }
    };

    public DefaultChromeClient(Activity activity, IndicatorController indicatorController, WebChromeClient chromeClient, VideoListener videoListener, PermissionInterceptor permissionInterceptor, WebView webView) {
        super(chromeClient);
        this.mIndicatorController = indicatorController;
        isWrapper = chromeClient != null ? true : false;
        this.mWebChromeClient = chromeClient;
        mActivityWeakReference = new WeakReference<>(activity);
        this.mVideoListener = videoListener;
        this.mPermissionInterceptor = permissionInterceptor;
        this.mWebView = webView;
        mWebUIController = new WeakReference<>(FastWebUtils.getFastWebUIController(webView));
    }

    @Override
    public void onProgressChanged(WebView view, int newProgress) {
        super.onProgressChanged(view, newProgress);
        if (mIndicatorController != null) {
            mIndicatorController.progress(view, newProgress);
        }
    }

    @Override
    public void onReceivedTitle(WebView view, String title) {
        if (isWrapper) {
            super.onReceivedTitle(view, title);
        }
    }

    @Override
    public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
        if (mWebUIController.get() != null) {
            mWebUIController.get().onJsAlert(view, url, message);
        }
        result.confirm();
        return true;
    }

    @Override
    public void onReceivedIcon(WebView view, Bitmap icon) {
        super.onReceivedIcon(view, icon);
    }

    @Override
    public void onGeolocationPermissionsHidePrompt() {
        super.onGeolocationPermissionsHidePrompt();
    }

    @Override
    public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
        onGeolocationPermissionsShowPromptInternal(origin, callback);
    }

    private void onGeolocationPermissionsShowPromptInternal(String origin, GeolocationPermissions.Callback callback) {
        if (mPermissionInterceptor != null) {
            if (mPermissionInterceptor.intercept(mWebView.getUrl(), FastWebPermissions.LOCATION, "location")) {
                callback.invoke(origin, false, false);
                return;
            }
        }
        Activity activity = mActivityWeakReference.get();
        if (activity == null) {
            callback.invoke(origin, false, false);
            return;
        }
        List<String> deniedPermissions;
        if ((deniedPermissions = FastWebUtils.getDeniedPermissions(activity, FastWebPermissions.LOCATION)).isEmpty()) {
            callback.invoke(origin, true, false);
        } else {
            Action action = Action.createPermissionsAction(deniedPermissions.toArray(new String[]{}));
            action.setFromIntention(FROM_CODE_INTENTION_LOCATION);
            ActionActivity.setPermissionListener(mPermissionListener);
            this.mCallBack = callback;
            this.mOrigin = origin;
            ActionActivity.start(activity, action);
        }
    }

    @Override
    public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
        try {
            if (mWebUIController.get() != null) {
                mWebUIController.get().onJsPrompt(mWebView, url, message, defaultValue, result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
        if (mWebUIController.get() != null) {
            mWebUIController.get().onJsConfirm(view, url, message, result);
        }
        return true;
    }

    @Override
    public void onExceededDatabaseQuota(String url, String databaseIdentifier, long quota, long estimatedDatabaseSize, long totalQuota, WebStorage.QuotaUpdater quotaUpdater) {
        quotaUpdater.updateQuota(totalQuota * 2);
    }

    @Override
    public void onReachedMaxAppCacheSize(long requiredStorage, long quota, WebStorage.QuotaUpdater quotaUpdater) {
        quotaUpdater.updateQuota(requiredStorage * 2);
    }

    @Override
    public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
        return openFileChooserAboveL(webView, filePathCallback, fileChooserParams);
    }

    private boolean openFileChooserAboveL(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
        Activity activity = mActivityWeakReference.get();
        if (activity == null || activity.isFinishing()) {
            return false;
        }
        return FastWebUtils.showFileChooserCompat(activity, mWebView, filePathCallback, fileChooserParams, mPermissionInterceptor, null, null, null);
    }

    @Override
    public void openFileChooser(ValueCallback<Uri> valueCallback) {
        createAndOpenCommonFileChooser(valueCallback, "*/*");
    }

    @Override
    public void openFileChooser(ValueCallback valueCallback, String acceptType) {
        createAndOpenCommonFileChooser(valueCallback, acceptType);
    }

    @Override
    public void openFileChooser(ValueCallback<Uri> uploadFile, String acceptType, String capture) {
        createAndOpenCommonFileChooser(uploadFile, acceptType);
    }

    private void createAndOpenCommonFileChooser(ValueCallback valueCallback, String acceptType) {
        Activity activity = mActivityWeakReference.get();
        if (activity == null && activity.isFinishing()) {
            valueCallback.onReceiveValue(new Object());
            return;
        }
        FastWebUtils.showFileChooserCompat(activity,
                mWebView,
                null,
                null,
                mPermissionInterceptor,
                valueCallback,
                acceptType,
                null);
    }

    @Override
    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        return true;
    }

    @Override
    public void onShowCustomView(View view, CustomViewCallback callback) {
        if(mVideoListener != null) {
            mVideoListener.onShowCustomView(view, callback);
        }
    }

    @Override
    public void onHideCustomView() {
        if (mVideoListener != null) {
            mVideoListener.onHideCustomView();
        }
    }
}
