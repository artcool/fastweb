package com.artcool.fastweb.core.creator.listener;

import com.artcool.fastweb.core.base.listener.BaseIndicatorSpecListener;

/**
 *
 * @author wuyibin
 * @date 2019/5/5
 */
public interface WebIndicatorListener<T extends BaseIndicatorSpecListener> {
    T offer();
}
