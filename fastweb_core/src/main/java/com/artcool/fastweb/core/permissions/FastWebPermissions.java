package com.artcool.fastweb.core.permissions;


import android.Manifest;

/**
 *
 * @author wuyibin
 * @date 2019/5/23
 */
public class FastWebPermissions {
    public static String[] CAMERA;
    public static String[] LOCATION;
    public static String[] STORAGE;
    public static final String ACTION_CAMERA = "Camera";
    public static final String ACTION_LOCATION = "Location";
    public static final String ACTION_STORAGE = "Storage";

    static {
        CAMERA = new String[]{Manifest.permission.CAMERA};
        LOCATION = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION};
        STORAGE = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE};
    }
}
