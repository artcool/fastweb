package com.artcool.fastweb.core.controller;

import android.app.Activity;
import android.app.Dialog;
import android.os.Handler;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebView;

import com.artcool.fastweb.core.layout.WebParentLayout;

/**
 *  统一控制与用户交互界面的父类
 * @author wuyibin
 * @date 2019/5/5
 */
public abstract class AbsFastWebUIController {

    public static boolean HAS_DESIGN_LIB = false;
    private Activity mActivity;
    private WebParentLayout mWebParentLayout;
    private volatile boolean isBindWebParent = false;
    protected AbsFastWebUIController mAbsFastWebUIController;

    static {
        try {
            Class.forName("android.support.design.widget.Snackbar");
            Class.forName("android.support.design.widget.BottomSheetDialog");
            HAS_DESIGN_LIB = true;
        }catch (Exception e) {
            HAS_DESIGN_LIB = false;
        }
    }

    protected AbsFastWebUIController create() {
        return HAS_DESIGN_LIB ? new DefaultDesignUiController() : new DefaultUiController();
    }

    protected AbsFastWebUIController getDelegate() {
        AbsFastWebUIController controller = mAbsFastWebUIController;
        if (controller == null) {
            mAbsFastWebUIController = controller = create();
        }
        return mAbsFastWebUIController;
    }

    public final synchronized void bindWebParent(WebParentLayout webParentLayout,Activity activity) {
        if (!isBindWebParent) {
            isBindWebParent = true;
            this.mWebParentLayout = webParentLayout;
            this.mActivity = activity;
            bindSupportWebParent(webParentLayout,activity);
        }
    }

    protected abstract void bindSupportWebParent(WebParentLayout webParentLayout, Activity activity);

    protected void toDismissDialog(Dialog dialog) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    protected void toShowDialog(Dialog dialog) {
        if (dialog != null && !dialog.isShowing()) {
            dialog.show();
        }
    }

    public abstract void onJsAlert(WebView webView,String url,String message);

    /**
     * 提示用户是否前往其他页面
     * @param webView
     * @param url
     * @param callback
     */
    public abstract void onOpenOtherPage(WebView webView, String url, Handler.Callback callback);

    public abstract void onJsConfirm(WebView webView, String url, String message, JsResult result);

    public abstract void onSelectItemsPrompt(WebView webView,String url,String[] ways,Handler.Callback callback);

    /**
     * 强制显示下载弹窗
     * @param url
     * @param callback
     */
    public abstract void onForceDownloadAlert(String url,Handler.Callback callback);

    public abstract void onJsPrompt(WebView webView, String url, String message, String defaultValue, JsPromptResult result);

    /**
     * 显示错误页
     * @param webView
     * @param errCode
     * @param description
     * @param failingUrl
     */
    public abstract void onShowErrorPage(WebView webView,int errCode,String description,String failingUrl);

    /**
     * 隐藏错误页
     */
    public abstract void onHideErrorPage();

    /**
     * 正在加载页面内容
     * @param msg
     */
    public abstract void onLoading(String msg);

    /**
     * 取消正在加载
     */
    public abstract void onCancelLoading();

    /**
     * 显示消息
     * @param message
     * @param intent
     */
    public abstract void onShowMessage(String message,String intent);

    /**
     * 权限被拒绝
     * @param permissions
     * @param permissionType
     * @param action
     */
    public abstract void onPermissionDeny(String[] permissions,String permissionType,String action);


}
