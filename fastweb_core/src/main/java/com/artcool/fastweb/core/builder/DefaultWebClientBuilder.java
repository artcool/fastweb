package com.artcool.fastweb.core.builder;

import android.app.Activity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.artcool.fastweb.core.client.DefaultWebClient;
import com.artcool.fastweb.core.interceptor.PermissionInterceptor;

/**
 *
 * @author wuyibin
 * @date 2019/5/6
 */
public class DefaultWebClientBuilder {
    private Activity mActivity;
    private WebViewClient mWebViewClient;
    private boolean mWebClientHelper;
    private PermissionInterceptor mPermissionInterceptor;
    private WebView mWebView;
    private boolean isInterceptUnkownScheme;
    private int mUrlHandleWays;

    public Activity getActivity() {
        return mActivity;
    }

    public WebViewClient getWebViewClient() {
        return mWebViewClient;
    }

    public boolean isWebClientHelper() {
        return mWebClientHelper;
    }

    public PermissionInterceptor getPermissionInterceptor() {
        return mPermissionInterceptor;
    }

    public WebView getWebView() {
        return mWebView;
    }

    public boolean isInterceptUnkownScheme() {
        return isInterceptUnkownScheme;
    }

    public int getUrlHandleWays() {
        return mUrlHandleWays;
    }

    public DefaultWebClientBuilder setActivity(Activity mActivity) {
        this.mActivity = mActivity;
        return this;
    }

    public DefaultWebClientBuilder setWebViewClient(WebViewClient mWebViewClient) {
        this.mWebViewClient = mWebViewClient;
        return this;
    }

    public DefaultWebClientBuilder setWebClientHelper(boolean mWebClientHelper) {
        this.mWebClientHelper = mWebClientHelper;
        return this;
    }

    public DefaultWebClientBuilder setPermissionInterceptor(PermissionInterceptor mPermissionInterceptor) {
        this.mPermissionInterceptor = mPermissionInterceptor;
        return this;
    }

    public DefaultWebClientBuilder setWebView(WebView mWebView) {
        this.mWebView = mWebView;
        return this;
    }

    public DefaultWebClientBuilder setInterceptUnkownScheme(boolean interceptUnkownScheme) {
        isInterceptUnkownScheme = interceptUnkownScheme;
        return this;
    }

    public DefaultWebClientBuilder setUrlHandleWays(int mUrlHandleWays) {
        this.mUrlHandleWays = mUrlHandleWays;
        return this;
    }

    private DefaultWebClient build() {
        return new DefaultWebClient(this);
    }
}
