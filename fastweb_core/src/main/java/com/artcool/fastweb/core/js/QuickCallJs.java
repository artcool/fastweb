package com.artcool.fastweb.core.js;

import android.webkit.ValueCallback;

/**
 *
 * @author wuyibin
 * @date 2019/5/21
 */
public interface QuickCallJs {

    /**
     * 调用js方法
     * @param methodName    方法名
     * @param callback  回调
     * @param params    参数
     */
    void quickCallJs(String methodName, ValueCallback<String> callback,String... params);

    void quickCallJs(String methodName,String... params);

    void quickCallJs(String methodName);


}
