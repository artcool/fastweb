package com.artcool.fastweb.core.layout;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.webkit.WebView;
import android.widget.FrameLayout;

import com.artcool.fastweb.core.R;
import com.artcool.fastweb.core.controller.AbsFastWebUIController;
import com.artcool.fastweb.core.layout.listener.Provider;

/**
 *
 * @author wuyibin
 * @date 2019/5/5
 */
public class WebParentLayout extends FrameLayout implements Provider<AbsFastWebUIController> {
    private AbsFastWebUIController mAbsFastWebUIController = null;
    @LayoutRes
    private int mErrorLayoutRes;
    @IdRes
    private int mClickId = -1;
    private View mErrorView;
    private WebView mWebView;
    private FrameLayout mErrorLayout = null;
    public WebParentLayout( @NonNull Context context) {
        super(context);
    }

    public WebParentLayout(@NonNull Context context,@Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public WebParentLayout(@NonNull Context context,@Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!(context instanceof Activity)) {
            throw new IllegalArgumentException("WebParentLayout context must be from activity or activity sub class .");
        }
        mErrorLayoutRes = R.layout.error_page;
    }

    public final void bindController(AbsFastWebUIController controller) {
        this.mAbsFastWebUIController = controller;
        this.mAbsFastWebUIController.bindWebParent(this, (Activity) getContext());
    }

    public final void showErrorPage() {
        View container = this.mErrorLayout;
        if (container != null) {
            container.setVisibility(VISIBLE);
        } else {
            createErrorLayout();
            container = this.mErrorLayout;
        }
        View clickView = null;
        if (mClickId != -1 && (clickView = container.findViewById(mClickId)) != null) {
            clickView.setClickable(true);
        } else {
            container.setClickable(true);
        }
    }

    public final void hideErrorLayout() {
        View mView = null;
        if ((mView = this.findViewById(R.id.mainframe_error_container_id)) != null) {
            mView.setVisibility(View.GONE);
        }
    }

    public final void setErrorView(@NonNull View errorView) {
        this.mErrorView = errorView;
    }

    public final void setErrorLayoutRes(@LayoutRes int resLayout,@IdRes int id) {
        this.mClickId = id;
        if (mClickId <= 0) {
            mClickId = -1;
        }
        this.mErrorLayoutRes = resLayout;
        if (mErrorLayoutRes <= 0) {
            mErrorLayoutRes = R.layout.error_page;
        }
    }

    private void createErrorLayout() {
        final FrameLayout mFrameLayout = new FrameLayout(getContext());
        mFrameLayout.setBackgroundColor(Color.WHITE);
        mFrameLayout.setId(R.id.mainframe_error_container_id);
        if (this.mErrorView == null) {
            LayoutInflater mInflater = LayoutInflater.from(getContext());
            mInflater.inflate(mErrorLayoutRes,mFrameLayout,true);
        } else {
            mFrameLayout.addView(mErrorView);
        }

        ViewStub mStub = this.findViewById(R.id.mainframe_error_viewsub_id);
        final int index = this.indexOfChild(mStub);
        this.removeViewInLayout(mStub);
        final ViewGroup.LayoutParams params = getLayoutParams();
        if (params != null) {
            this.addView(this.mErrorLayout = mFrameLayout,index,params);
        } else {
            this.addView(this.mErrorLayout = mFrameLayout,index);
        }
        mFrameLayout.setVisibility(View.VISIBLE);
        if (mClickId != -1) {
            final View clickView = mFrameLayout.findViewById(mClickId);
            if (clickView != null) {
                clickView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (getWebView() != null) {
                            clickView.setClickable(false);
                            getWebView().reload();
                        }
                    }
                });
                return;
            } else {
                throw new NullPointerException("Click View is null");
            }
        }
        mFrameLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getWebView() != null) {
                    mFrameLayout.setClickable(false);
                    getWebView().reload();
                }
            }
        });
    }

    @Override
    public AbsFastWebUIController provide() {
        return this.mAbsFastWebUIController;
    }

    public WebView getWebView() {
        return this.mWebView;
    }

    public final void bindWebView(WebView webView) {
        if (this.mWebView == null) {
            this.mWebView = webView;
        }
    }
}
