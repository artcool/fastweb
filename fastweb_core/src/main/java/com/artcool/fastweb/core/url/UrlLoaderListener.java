package com.artcool.fastweb.core.url;

import com.artcool.fastweb.core.http.HttpHeaders;

import java.util.Map;

/**
 *
 * @author wuyibin
 * @date 2019/5/21
 */
public interface UrlLoaderListener {

    void loadUrl(String url);

    void loadUrl(String url, Map<String,String> headers);

    void reload();

    void loadData(String data,String mimeType,String encoding);

    void stopLoading();

    void loadDataWithBaseUrl(String baseUrl,String data,String mimeType,String encoding,String historyUrl);

    void postUrl(String url,byte[] params);

    HttpHeaders getHttpHeaders();
}
