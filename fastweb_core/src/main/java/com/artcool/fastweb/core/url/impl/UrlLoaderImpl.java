package com.artcool.fastweb.core.url.impl;

import android.os.Handler;
import android.os.Looper;
import android.webkit.WebView;

import com.artcool.fastweb.core.http.HttpHeaders;
import com.artcool.fastweb.core.url.UrlLoaderListener;
import com.artcool.fastweb.core.utils.FastWebUtils;

import java.util.Map;


/**
 * @author wuyibin
 * @date 2019/5/21
 */
public class UrlLoaderImpl implements UrlLoaderListener {

    private Handler mHandler = null;
    private WebView mWebView;
    private HttpHeaders mHttpHeaders;

    public UrlLoaderImpl(WebView webView, HttpHeaders httpHeaders) {
        this.mWebView = webView;
        if (this.mWebView == null) {
            throw new NullPointerException("webView 不能为空!");
        }
        this.mHttpHeaders = httpHeaders;
        if (mHttpHeaders == null) {
            mHttpHeaders = HttpHeaders.create();
        }

        mHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void loadUrl(String url) {
        this.loadUrl(url, mHttpHeaders.getHeaders(url));
    }

    @Override
    public void loadUrl(final String url, final Map<String, String> headers) {
        if (!FastWebUtils.isUIThread()) {
            FastWebUtils.runInUiThread(new Runnable() {
                @Override
                public void run() {
                    loadUrl(url, headers);
                }
            });
        }

        if (headers == null || headers.isEmpty()) {
            mWebView.loadUrl(url);
        } else {
            mWebView.loadUrl(url, headers);
        }
    }

    @Override
    public void reload() {
        if (!FastWebUtils.isUIThread()) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    reload();
                }
            });
            return;
        }
        mWebView.reload();
    }

    @Override
    public void loadData(final String data, final String mimeType, final String encoding) {
        if (!FastWebUtils.isUIThread()) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    loadData(data, mimeType, encoding);
                }
            });
            return;
        }
        mWebView.loadData(data, mimeType, encoding);
    }

    @Override
    public void stopLoading() {
        if (!FastWebUtils.isUIThread()) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    stopLoading();
                }
            });
            return;
        }
        mWebView.stopLoading();
    }

    @Override
    public void loadDataWithBaseUrl(final String baseUrl, final String data, final String mimeType, final String encoding, final String historyUrl) {
        if (!FastWebUtils.isUIThread()) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    loadDataWithBaseUrl(baseUrl, data, mimeType, encoding, historyUrl);
                }
            });
            return;
        }
        mWebView.loadDataWithBaseURL(baseUrl, data, mimeType, encoding, historyUrl);
    }

    @Override
    public void postUrl(final String url, final byte[] params) {
        if (!FastWebUtils.isUIThread()) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    postUrl(url, params);
                }
            });
            return;
        }
        mWebView.postUrl(url, params);
    }

    @Override
    public HttpHeaders getHttpHeaders() {
        return mHttpHeaders == null ? mHttpHeaders = HttpHeaders.create() : mHttpHeaders;
    }
}
