package com.artcool.fastweb.core.js;

import android.os.Build;
import android.webkit.JavascriptInterface;

import com.artcool.fastweb.core.config.FastWebConfig;
import com.artcool.fastweb.core.security.SecurityType;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 *
 * @author wuyibin
 * @date 2019/5/22
 */
public abstract class BaseJsInterfaceHolder implements JsInterfaceHolder {
    private SecurityType mSecurityType;

    protected BaseJsInterfaceHolder(SecurityType type) {
        this.mSecurityType = type;
    }

    @Override
    public boolean checkObject(Object object) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return true;
        }
        if (FastWebConfig.WEBVIEW_TYPE == FastWebConfig.WEBVIEW_FASTWEB_SAFE_TYPE) {
            return true;
        }
        boolean tag = false;
        Class<?> claszz = object.getClass();
        Method[] methods = claszz.getMethods();
        for (Method method : methods) {
            Annotation[] annotations = method.getAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation instanceof JavascriptInterface) {
                    tag = true;
                    break;
                }
            }
            if (tag) {
                break;
            }
        }
        return tag;
    }

    protected boolean checkScurity() {
        return mSecurityType != SecurityType.STRICT ? true : FastWebConfig.WEBVIEW_TYPE == FastWebConfig.WEBVIEW_FASTWEB_SAFE_TYPE ? true : Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR1;
    }
}
