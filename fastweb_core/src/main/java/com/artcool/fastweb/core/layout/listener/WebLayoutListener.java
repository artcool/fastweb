package com.artcool.fastweb.core.layout.listener;

import android.support.annotation.NonNull;
import android.view.ViewGroup;
import android.webkit.WebView;

/**
 *
 * @author wuyibin
 * @date 2019/5/5
 */
public interface WebLayoutListener<T extends WebView,V extends ViewGroup> {

    /**
     * webview 父控件
     * @return
     */
    @NonNull V getLayout();

    /**
     * 返回webview或webView子view
     * @return
     */
    @NonNull T getWebView();
}
