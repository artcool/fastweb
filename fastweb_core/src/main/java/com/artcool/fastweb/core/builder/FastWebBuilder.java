package com.artcool.fastweb.core.builder;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.artcool.fastweb.core.FastWeb;
import com.artcool.fastweb.core.base.BaseIndicatorView;
import com.artcool.fastweb.core.base.BaseMiddleWebChrome;
import com.artcool.fastweb.core.base.BaseMiddlewareWebClient;
import com.artcool.fastweb.core.client.FastWebChromeClient;
import com.artcool.fastweb.core.client.FastWebViewClient;
import com.artcool.fastweb.core.controller.AbsFastWebUIController;
import com.artcool.fastweb.core.controller.IndicatorController;
import com.artcool.fastweb.core.creator.WebViewCreator;
import com.artcool.fastweb.core.enums.OpenOtherPageWays;
import com.artcool.fastweb.core.event.EventHandlerListener;
import com.artcool.fastweb.core.http.HttpHeaders;
import com.artcool.fastweb.core.interceptor.PermissionInterceptor;
import com.artcool.fastweb.core.layout.listener.WebLayoutListener;
import com.artcool.fastweb.core.manager.HookManager;
import com.artcool.fastweb.core.security.SecurityType;
import com.artcool.fastweb.core.settings.FastWebSettingListener;
import com.artcool.fastweb.core.web.PreFastWeb;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by wuyibin on 2019/5/5.
 */
public class FastWebBuilder {
    public Activity mActivity;
    public Fragment mFragment;
    public ViewGroup mViewGround;
    //是否需要默认进度条
    public boolean mIsDefaultProgress;
    public int mIndex = -1;
    public BaseIndicatorView mBaseIndicatorView;
    public IndicatorController mIndicatorController = null;
    /**
     * 显示默认进度条
     */
    public boolean mDefaultIndicatorEnable = true;
    public ViewGroup.LayoutParams mLayoutParams = null;
    /**
     * WEBVIEWCLIENT
     */
    public FastWebViewClient mFastWebViewClient;
    /**
     * WEBCHROMECLIENT
     */
    public FastWebChromeClient mFastWebChromeClient;
    /**
     * 进度条颜色
     */
    public int mIndicatorColor = -1;
    /**
     * web设置
     */
    public FastWebSettingListener mFastWebSettingListener;
    /**
     * webview建造器
     */
    public WebViewCreator mWebViewCreator;
    /**
     * http协议请求头
     */
    public HttpHeaders mHttpHeaders = null;
    /**
     * 事件处理监听
     */
    public EventHandlerListener mEventHandlerListener;
    public int mHeight = -1;
    public LinkedHashMap<String,Object> mJavaObject;
    /**
     * 安全类型
     */
    public SecurityType mSecurityType = SecurityType.DEFAULT;
    public WebView mWebView;
    public boolean mWebClientHelper = true;
    public WebLayoutListener mWebLayoutListener = null;
    /**
     * 权限过滤器
     */
    public PermissionInterceptor mPermissionInterceptor = null;
    public AbsFastWebUIController mWebUIController;
    /**
     * 打开其他页面
     */
    public OpenOtherPageWays mOpenOtherPage = null;
    /**
     * 是否拦截未知url    默认false
     */
    public boolean isInterceptUnkownUrl = false;
    public BaseMiddlewareWebClient mBaseMiddlewareWebClientHeader;
    public BaseMiddlewareWebClient mBaseMiddlewareWebClientTail;
    public BaseMiddleWebChrome mBaseMiddleWebChromeHeader = null;
    public BaseMiddleWebChrome mBaseMiddleWebChromeTail = null;
    public View mErrorView;
    public int mErrorLayout;
    public int mReloadId;
    public int mTag;

    public FastWebBuilder(@NonNull Activity activity,@NonNull Fragment fragment) {
        this.mActivity = activity;
        this.mFragment = fragment;
        mTag = FastWeb.FRAGMENT_TAG;
    }

    public FastWebBuilder(@NonNull Activity activity) {
        this.mActivity = activity;
        mTag = FastWeb.ACTIVITY_TAG;
    }

    public final void addJavaObject(String key,Object o) {
        if (mJavaObject == null) {
            mJavaObject = new LinkedHashMap<>();
        }
        mJavaObject.put(key, o);
    }

    public final void addHeader(String baseUrl,String key,String value) {
        if (mHttpHeaders == null) {
            mHttpHeaders = HttpHeaders.create();
        }
        mHttpHeaders.addHttpHeader(baseUrl, key, value);
    }

    public final void addHeader(String baseUrl, Map<String,String> headers) {
        if (mHttpHeaders == null) {
            mHttpHeaders = HttpHeaders.create();
        }
        mHttpHeaders.addHttpHeaders(baseUrl, headers);
    }

    public final PreFastWeb buildFastWeb() {
        if (mTag == FastWeb.FRAGMENT_TAG && this.mViewGround == null) {
            throw new NullPointerException("ViewGroup is null,Please check your parameters .");
        }
        return new PreFastWeb(HookManager.hookFastWeb(new FastWeb(this),this));
    }

    public final IndicatorBuilder setFastWebParent(@NonNull ViewGroup viewGroup,@NonNull ViewGroup.LayoutParams params) {
        this.mViewGround = viewGroup;
        this.mLayoutParams = params;
        return new IndicatorBuilder(this);
    }

    public final IndicatorBuilder setFastWebParent(@NonNull ViewGroup viewGroup,int index,@NonNull ViewGroup.LayoutParams params) {
        this.mViewGround = viewGroup;
        this.mLayoutParams = params;
        this.mIndex = index;
        return new IndicatorBuilder(this);
    }


}
