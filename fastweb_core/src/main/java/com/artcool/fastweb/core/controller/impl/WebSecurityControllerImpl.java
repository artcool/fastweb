package com.artcool.fastweb.core.controller.impl;

import android.os.Build;
import android.webkit.WebView;

import com.artcool.fastweb.core.controller.WebScurityController;
import com.artcool.fastweb.core.controller.WebSecurityCheckLogicController;
import com.artcool.fastweb.core.security.SecurityType;

import java.util.LinkedHashMap;

/**
 *
 * @author wuyibin
 * @date 2019/5/21
 */
public class WebSecurityControllerImpl implements WebScurityController<WebSecurityCheckLogicController> {

    private WebView mWebView;
    private LinkedHashMap<String,Object> mMap;
    private SecurityType type;

    public WebSecurityControllerImpl(WebView webView, LinkedHashMap<String, Object> map, SecurityType securityType) {
        this.mWebView = webView;
        this.mMap = map;
        this.type = securityType;
    }

    @Override
    public void check(WebSecurityCheckLogicController webSecurityCheckLogicController) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
            webSecurityCheckLogicController.dealHoneyComb(mWebView);
        }
        if (mMap != null && type == SecurityType.STRICT && mMap.isEmpty()) {
            webSecurityCheckLogicController.dealJsInterface(mMap,type);
        }
    }
}
