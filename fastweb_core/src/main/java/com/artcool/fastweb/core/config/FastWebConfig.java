package com.artcool.fastweb.core.config;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.ValueCallback;
import android.webkit.WebView;

import com.artcool.fastweb.core.utils.FastWebUtils;

import java.io.File;

/**
 * @author wuyibin
 * @date 2019/5/14
 */
public class FastWebConfig {

    /**
     * 默认webView类型
     */
    public static final int WEBVIEW_DEFAULT_TYPE = 1;
    /**
     * 使用fastwebView
     */
    public static final int WEBVIEW_FASTWEB_SAFE_TYPE = 2;
    /**
     * 版本相关
     */
    public static final String FASTWEB_VERSION = "fastweb/1.0.0";
    public static final String FASTWEB_NAME = "FastWeb";
    public static final String FILE_CACHE_PATH = "fast_web_cache";
    private static final String FAST_WEB_CACHE_PATH = File.separator + FILE_CACHE_PATH;
    /**
     * 当前操作系统是否低于kitkat
     */
    public static final boolean IS_KITKAT_OR_BELOW_KITKAT = Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT;
    /**
     * 自定义webview
     */
    public static final int WEBVIEW_CUSTOM_TYPE = 3;
    /**
     * debug模式，如果需要查看日志请设置为true
     */
    public static boolean DEBUG = false;
    /**
     * 通过js获取的文件大小，这里限制最大为5MB，太大会抛出oom
     */
    public static int MAX_FILE_LENGTH = 1024 * 1024 * 5;
    /**
     * 缓存路径
     */
    public static String FAST_WEB_FILE_PATH;
    public static int WEBVIEW_TYPE = WEBVIEW_DEFAULT_TYPE;
    private static volatile boolean IS_INITIALIZE = false;

    /**
     * 获取cookie
     *
     * @param url
     * @return
     */
    public static String getCookieByUrl(String url) {
        return CookieManager.getInstance() == null ? null : CookieManager.getInstance().getCookie(url);
    }

    /**
     * 打开debug模式
     */
    public static void debug() {
        DEBUG = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
    }

    /**
     * 删除所有过期的Cookies
     */
    public static void removeExpiredCookies() {
        CookieManager manager;
        if ((manager=CookieManager.getInstance()) != null) {
            manager.removeExpiredCookie();
            toSynCookies();
        }
    }

    /**
     * 同步cookie
     */
    private static void toSynCookies() {
        if (Build.VERSION.SDK_INT<Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.getInstance().sync();
            return;
        }
        AsyncTask.THREAD_POOL_EXECUTOR.execute(new Runnable() {
            @Override
            public void run() {
                CookieManager.getInstance().flush();
            }
        });
    }

    /**
     * 删除所有Cookie
     */
    public static void removeAllCookies() {
        removeAllCookies(null);
    }

    /**
     * 解决兼容 Android 4.4 java.lang.NoSuchMethodError: android.webkit.CookieManager.removeSessionCookies
     */
    public static void removeSessionCookies() {
        removeSessionCookies(null);
    }

    private static void removeSessionCookies(ValueCallback<Boolean> callback) {
        if (callback == null) {
            callback = getDefaultIgnoreCallback();
        }
        if (CookieManager.getInstance() == null) {
            callback.onReceiveValue(new Boolean(false));
            return;
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().removeSessionCookie();
            toSynCookies();
            callback.onReceiveValue(new Boolean(true));
            return;
        }
        CookieManager.getInstance().removeAllCookies(callback);
        toSynCookies();
    }

    /**
     * 删除所有cookie
     * @param callback
     */
    public static void removeAllCookies(@Nullable ValueCallback<Boolean> callback) {
        if (callback == null) {
            callback = getDefaultIgnoreCallback();
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().removeAllCookie();
            toSynCookies();
            callback.onReceiveValue(!CookieManager.getInstance().hasCookies());
            return;
        }
        CookieManager.getInstance().removeAllCookies(callback);
        toSynCookies();
    }

    /**
     * 获取webview的缓存路径
     * @param context
     * @return
     */
    public static String getCachePath(Context context) {
        return context.getCacheDir().getAbsolutePath() + FAST_WEB_CACHE_PATH;
    }

    public static String getExternalCachePath(Context context) {
        return FastWebUtils.getFastWebFilePath(context);
    }


    private static ValueCallback<Boolean> getDefaultIgnoreCallback() {
        return new ValueCallback<Boolean>() {
            @Override
            public void onReceiveValue(Boolean value) {
                Log.i("FastWeb", "onReceiveValue: " + value);
            }
        };
    }

    public static synchronized void initCookiesManager(Context context) {
        if (!IS_INITIALIZE) {
            createCookiesSyncInstance(context);
            IS_INITIALIZE = true;
        }
    }

    private static void createCookiesSyncInstance(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.getInstance().sync();
            return;
        }
        AsyncTask.THREAD_POOL_EXECUTOR.execute(new Runnable() {
            @Override
            public void run() {
                CookieManager.getInstance().flush();
            }
        });
    }

}
