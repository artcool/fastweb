package com.artcool.fastweb.core;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.artcool.fastweb.core.base.BaseIndicatorView;
import com.artcool.fastweb.core.base.BaseMiddleWebChrome;
import com.artcool.fastweb.core.base.BaseMiddlewareWebClient;
import com.artcool.fastweb.core.builder.FastWebBuilder;
import com.artcool.fastweb.core.client.DefaultChromeClient;
import com.artcool.fastweb.core.compat.FastWebJsInterfaceCompat;
import com.artcool.fastweb.core.config.FastWebConfig;
import com.artcool.fastweb.core.controller.IndicatorController;
import com.artcool.fastweb.core.controller.WebScurityController;
import com.artcool.fastweb.core.controller.WebSecurityCheckLogicController;
import com.artcool.fastweb.core.controller.impl.FastWebUIControllerImplBase;
import com.artcool.fastweb.core.controller.impl.WebSecurityCheckLogicControllerImpl;
import com.artcool.fastweb.core.controller.impl.WebSecurityControllerImpl;
import com.artcool.fastweb.core.creator.DefaultWebViewCreator;
import com.artcool.fastweb.core.creator.WebViewCreator;
import com.artcool.fastweb.core.event.EventHandlerListener;
import com.artcool.fastweb.core.event.impl.EventHandlerImpl;
import com.artcool.fastweb.core.indicator.IndicatorHandler;
import com.artcool.fastweb.core.interceptor.EventInterceptor;
import com.artcool.fastweb.core.interceptor.PermissionInterceptor;
import com.artcool.fastweb.core.js.JsAccessEntrance;
import com.artcool.fastweb.core.js.JsInterfaceHolder;
import com.artcool.fastweb.core.js.impl.JsAccessEntranceImpl;
import com.artcool.fastweb.core.js.impl.JsInterfaceHolderImpl;
import com.artcool.fastweb.core.layout.WebParentLayout;
import com.artcool.fastweb.core.layout.listener.WebLayoutListener;
import com.artcool.fastweb.core.security.SecurityType;
import com.artcool.fastweb.core.settings.AbsFastWebSettings;
import com.artcool.fastweb.core.settings.FastWebSettingListener;
import com.artcool.fastweb.core.settings.impl.FastWebSettingsImpl;
import com.artcool.fastweb.core.url.UrlLoaderListener;
import com.artcool.fastweb.core.url.impl.UrlLoaderImpl;
import com.artcool.fastweb.core.utils.FastWebUtils;
import com.artcool.fastweb.core.video.VideoListener;
import com.artcool.fastweb.core.video.impl.VideoImpl;
import com.artcool.fastweb.core.web.WebLifeCycle;
import com.artcool.fastweb.core.web.WebListenerManager;
import com.artcool.fastweb.core.web.impl.DefaultWebLifeCycleImpl;

import java.util.LinkedHashMap;

/**
 * @author wuyibin
 * @date 2019/5/5
 */
public final class FastWeb {

    /**
     * activity tag
     */
    public static final int ACTIVITY_TAG = 0;
    /**
     * fragment tag
     */
    public static final int FRAGMENT_TAG = 1;
    /**
     * Activity
     */
    private Activity mActivity;
    /**
     * 承载 WebParentLayout 的 ViewGroup
     */
    private ViewGroup mViewGround;
    /**
     * 处理webview相关的返回事件
     */
    private EventHandlerListener mEventListener;
    /**
     * 是否显示进度条
     */
    private boolean mEnableIndicator;
    /**
     * 负责创建布局 WebView ，WebParentLayout  Indicator等。
     */
    private WebViewCreator mWebViewCreator;
    private int TAG_TARGET = 0;
    private IndicatorController mIndicatorController;
    /**
     * WebChromeClient
     */
    private WebChromeClient mWebChromeClient;
    /**
     * WebViewClient
     */
    private WebViewClient mWebViewClient;
    /**
     * FastWeb
     */
    private FastWeb mFastWeb;
    /**
     *
     */
    private FastWebSettingListener mFastWebSettingListener;
    /**
     * webview注入对象
     */
    private LinkedHashMap<String, Object> mJavaObjects = new LinkedHashMap<>();
    /**
     * 权限拦截
     */
    private PermissionInterceptor mPermissionInterceptor;
    /**
     * 安全类型
     */
    private SecurityType mSecurityType = SecurityType.DEFAULT;
    /**
     * url加载监听
     */
    private UrlLoaderListener mUrlLoaderListener;
    /**
     * webview生命周期
     */
    private WebLifeCycle mWebLifeCycle;

    private WebScurityController<WebSecurityCheckLogicController> mWebScurityController;
    /**
     * webviewclient 辅助控制开关
     */
    private boolean mWebClientHelper;
    /**
     * 拦截未知url
     */
    private boolean isInterceptUnkownUrl;
    private int mUrlHandlerWays = -1;
    /**
     * WebViewClient 中间件
     */
    private BaseMiddlewareWebClient mBaseMiddlewareWebClientHeader;
    /**
     * WebChrome 中间件
     */
    private BaseMiddleWebChrome mBaseMiddleWebChromeHeader;
    /**
     * fastweb默认注入对象
     */
    private FastWebJsInterfaceCompat mFastWebJsInterfaceCompat;
    private WebSecurityCheckLogicController mWebSecurityCheckLogicController;
    /**
     * 提供快速JS方法调用
     */
    private JsAccessEntrance mJsAccessEntrance = null;
    /**
     * 事件处理监听
     */
    private EventHandlerListener mEventHandlerListener;
    /**
     * 事件拦截器
     */
    private EventInterceptor mEventInterceptor;
    private VideoListener mVideoListener;
    private WebListenerManager mWebListenerManager;
    /**
     * 注入对象管理
     */
    private JsInterfaceHolder mJsInterfaceHolder;

    private WebChromeClient mTargetChromeClient;

    public FastWeb(FastWebBuilder builder) {
        TAG_TARGET = builder.mTag;
        this.mActivity = builder.mActivity;
        this.mViewGround = builder.mViewGround;
        this.mEventListener = builder.mEventHandlerListener;
        this.mEnableIndicator = builder.mDefaultIndicatorEnable;
        mWebViewCreator = builder.mWebViewCreator == null ? configWebViewCreator(builder.mBaseIndicatorView, builder.mIndex, builder.mLayoutParams, builder.mIndicatorColor, builder.mHeight, builder.mWebView, builder.mWebLayoutListener) : builder.mWebViewCreator;
        this.mIndicatorController = builder.mIndicatorController;
        this.mWebChromeClient = builder.mFastWebChromeClient;
        this.mWebViewClient = builder.mFastWebViewClient;
        mFastWeb = this;
        this.mFastWebSettingListener = builder.mFastWebSettingListener;
        if (builder.mJavaObject != null && !builder.mJavaObject.isEmpty()) {
            this.mJavaObjects.putAll(builder.mJavaObject);
        }
        this.mPermissionInterceptor = builder.mPermissionInterceptor;
        this.mSecurityType = builder.mSecurityType;
        this.mUrlLoaderListener = new UrlLoaderImpl(mWebViewCreator.create().getWebView(), builder.mHttpHeaders);
        if (mWebViewCreator.getWebParenLayout() instanceof WebParentLayout) {
            WebParentLayout mWebParenLayout = (WebParentLayout) mWebViewCreator.getWebParenLayout();
            mWebParenLayout.bindController(builder.mWebUIController == null ? FastWebUIControllerImplBase.build() : builder.mWebUIController);
            mWebParenLayout.setErrorLayoutRes(builder.mErrorLayout, builder.mReloadId);
            mWebParenLayout.setErrorView(builder.mErrorView);
        }
        this.mWebLifeCycle = new DefaultWebLifeCycleImpl(mWebViewCreator.getWebView());
        this.mWebScurityController = new WebSecurityControllerImpl(mWebViewCreator.getWebView(), mJavaObjects, mSecurityType);
        this.mWebClientHelper = builder.mWebClientHelper;
        this.isInterceptUnkownUrl = builder.isInterceptUnkownUrl;
        if (builder.mOpenOtherPage != null) {
            this.mUrlHandlerWays = builder.mOpenOtherPage.code;
        }
        this.mBaseMiddlewareWebClientHeader = builder.mBaseMiddlewareWebClientHeader;
        this.mBaseMiddleWebChromeHeader = builder.mBaseMiddleWebChromeHeader;
        init();
    }

    public static FastWebBuilder with(@NonNull Activity activity) {
        if (activity == null) {
            throw new NullPointerException("activity 不能为空！");
        }
        return new FastWebBuilder(activity);
    }

    public static FastWebBuilder with(@NonNull Fragment fragment) {
        Activity activity;
        if ((activity = fragment.getActivity()) == null) {
            throw new NullPointerException("activity 不能为空！");
        }
        return new FastWebBuilder(activity, fragment);
    }

    private void init() {
        doCompat();
        doSafeCheck();
    }

    private void doSafeCheck() {
        WebSecurityCheckLogicController mCheckLogicController = this.mWebSecurityCheckLogicController;
        if (mCheckLogicController == null) {
            mWebSecurityCheckLogicController = mCheckLogicController = WebSecurityCheckLogicControllerImpl.getInstance();
        }
        mWebScurityController.check(mCheckLogicController);
    }

    private void doCompat() {
        mJavaObjects.put("fastWeb", mFastWebJsInterfaceCompat = new FastWebJsInterfaceCompat(this, mActivity));
    }

    private WebViewCreator configWebViewCreator(BaseIndicatorView mBaseIndicatorView, int mIndex, ViewGroup.LayoutParams mLayoutParams, int mIndicatorColor, int mHeight, WebView mWebView, WebLayoutListener mWebLayoutListener) {
        if (mBaseIndicatorView != null && mEnableIndicator) {
            return new DefaultWebViewCreator(mActivity, mViewGround, mLayoutParams, mIndex, mBaseIndicatorView, mWebView, mWebLayoutListener);
        } else {
            return mEnableIndicator ? new DefaultWebViewCreator(mActivity, mViewGround, mLayoutParams, mIndex, mIndicatorColor, mHeight, mWebView, mWebLayoutListener) : new DefaultWebViewCreator(mActivity, mViewGround, mLayoutParams, mIndex, mWebView, mWebLayoutListener);
        }
    }

    public PermissionInterceptor getPermissionInterceptor() {
        return mPermissionInterceptor;
    }

    public WebLifeCycle getWebLifeCycle() {
        return mWebLifeCycle;
    }

    public JsAccessEntrance getJsAccessEntrance() {
        JsAccessEntrance jsAccessEntrance = this.mJsAccessEntrance;
        if (jsAccessEntrance == null) {
            mJsAccessEntrance = jsAccessEntrance = JsAccessEntranceImpl.getInstance(mWebViewCreator.getWebView());
        }
        return jsAccessEntrance;
    }

    public WebViewCreator getWebViewCreator() {
        return mWebViewCreator;
    }

    public FastWeb clearWebCache() {
        if (getWebViewCreator().getWebView() != null) {
            FastWebUtils.clearWebViewAllCache(mActivity, getWebViewCreator().getWebView());
        } else {
            FastWebUtils.clearWebViewAllCache(mActivity);
        }
        return this;
    }

    public boolean handleKeyEvent(int keyCode, KeyEvent keyEvent) {
        if (mEventHandlerListener == null) {
            mEventHandlerListener = EventHandlerImpl.getInstance(mWebViewCreator.getWebView(), getEventInterceptor());
        }
        return mEventHandlerListener.onKeyDown(keyCode, keyEvent);
    }

    public boolean back() {
        if (mEventHandlerListener == null) {
            mEventHandlerListener = EventHandlerImpl.getInstance(mWebViewCreator.getWebView(),getEventInterceptor());
        }
        return mEventHandlerListener.back();
    }

    public EventHandlerListener getEventHandlerListener() {
        return mEventHandlerListener == null ? (this.mEventHandlerListener = EventHandlerImpl.getInstance(mWebViewCreator.getWebView(),getEventInterceptor())):this.mEventHandlerListener;
    }

    public FastWebSettingListener geFastWebSettingListener() {
        return this.mFastWebSettingListener;
    }

    public JsInterfaceHolder getJsInterfaceHolder() {
        return this.mJsInterfaceHolder;
    }


    private EventInterceptor getEventInterceptor() {
        if (mEventInterceptor != null) {
            return mEventInterceptor;
        }
        if (mVideoListener instanceof VideoImpl) {
            return this.mEventInterceptor = (EventInterceptor) this.mVideoListener;
        }
        return null;
    }

    private VideoListener getVideoListener() {
        return mVideoListener == null ? new VideoImpl(mActivity, mWebViewCreator.getWebView()) : mVideoListener;
    }

    public FastWeb ready() {
        FastWebConfig.initCookiesManager(mActivity.getApplicationContext());
        FastWebSettingListener fastWebSettingListener = this.mFastWebSettingListener;
        if (fastWebSettingListener == null) {
            mFastWebSettingListener = fastWebSettingListener = FastWebSettingsImpl.getInstance();
        }
        if (fastWebSettingListener instanceof AbsFastWebSettings) {
            ((AbsFastWebSettings) fastWebSettingListener).bindFastWeb(this);
        }
        if (mWebListenerManager == null && fastWebSettingListener instanceof AbsFastWebSettings) {
            mWebListenerManager = (WebListenerManager) fastWebSettingListener;
        }
        fastWebSettingListener.toSetting(mWebViewCreator.getWebView());
        if (mJsInterfaceHolder == null) {
            mJsInterfaceHolder = JsInterfaceHolderImpl.getJsInterfaceHolder(mWebViewCreator.getWebView(), mSecurityType);
        }
        if (mJavaObjects != null && !mJavaObjects.isEmpty()) {
            mWebListenerManager.setDownloader(mWebViewCreator.getWebView(), null);
            mWebListenerManager.setWebChromeClient(mWebViewCreator.getWebView(), getChromeClient());
            mWebListenerManager.setWebViewClient(mWebViewCreator.getWebView(), getWebViewClient());
        }
        return this;
    }

    private WebViewClient getWebViewClient() {
        return null;
    }

    private WebChromeClient getChromeClient() {
        IndicatorController mIndicatorController = this.mIndicatorController == null ? IndicatorHandler.getInstance().injectIndicator(mWebViewCreator.offer()) : this.mIndicatorController;
        DefaultChromeClient defaultChromeClient = new DefaultChromeClient(mActivity,
                this.mIndicatorController = mIndicatorController,
                this.mWebChromeClient, this.mVideoListener = getVideoListener(),
                this.mPermissionInterceptor, mWebViewCreator.getWebView());
        BaseMiddleWebChrome header = this.mBaseMiddleWebChromeHeader;
        if (header != null) {
            BaseMiddleWebChrome tail = header;
            int count = 1;
            BaseMiddleWebChrome temp = header;
            for (; temp.next() != null; ) {
                tail = temp = temp.next();
                count++;
            }
            tail.setDelegate(defaultChromeClient);
            return this.mTargetChromeClient = header;
        } else {
            return this.mTargetChromeClient = defaultChromeClient;
        }
    }

    public UrlLoaderListener getUrlLoader(){
        return mUrlLoaderListener;
    }

    public IndicatorController getIndicatorController() {
        return this.mIndicatorController;
    }

    public FastWeb go(String url) {
        this.getUrlLoader().loadUrl(url);
        IndicatorController indicatorController;
        if (!TextUtils.isEmpty(url) && (indicatorController = getIndicatorController()) != null && indicatorController.offerIndicator() != null) {
            getIndicatorController().offerIndicator().show();
        }
        return this;
    }
}
