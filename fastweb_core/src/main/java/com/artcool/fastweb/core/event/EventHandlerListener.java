package com.artcool.fastweb.core.event;

import android.view.KeyEvent;

/**
 *  时间处理监听
 * @author wuyibin
 * @date 2019/5/5
 */
public interface EventHandlerListener {
    /**
     * 键盘按下操作
     * @param keyCode
     * @param event
     * @return
     */
    boolean onKeyDown(int keyCode, KeyEvent event);

    /**
     * 返回
     * @return
     */
    boolean back();
}
