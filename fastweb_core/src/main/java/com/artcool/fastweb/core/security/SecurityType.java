package com.artcool.fastweb.core.security;

/**
 *
 * @author wuyibin
 * @date 2019/5/5
 */
public enum  SecurityType {
    DEFAULT,
    STRICT
}
