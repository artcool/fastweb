package com.artcool.fastweb.core.manager;

import com.artcool.fastweb.core.FastWeb;
import com.artcool.fastweb.core.builder.FastWebBuilder;

/**
 *
 * @author wuyibin
 * @date 2019/5/10
 */
public class HookManager {
    public static FastWeb hookFastWeb(FastWeb fastWeb, FastWebBuilder builder) {
        return fastWeb;
    }

    public static boolean permissionHook(String url,String[] permissions) {
        return true;
    }
}
