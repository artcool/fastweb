package com.artcool.fastweb.core.base.listener;

/**
 *
 * @author wuyibin
 * @date 2019/5/5
 */
public interface BaseIndicatorSpecListener {

    /**
     * 显示
     */
    void show();

    /**
     * 隐藏
     */
    void hide();

    /**
     * 重置
     */
    void reset();

    /**
     * 设置进度条
     * @param newProgress
     */
    void setProgress(int newProgress);
}
