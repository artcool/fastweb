package com.artcool.fastweb.core.client;

import com.artcool.fastweb.core.client.delegate.FastWebChromeClientDelegate;

/**
 *
 * @author wuyibin
 * @date 2019/5/5
 */
public class FastWebChromeClient extends FastWebChromeClientDelegate {

    public FastWebChromeClient() {
        super(null);
    }
}
