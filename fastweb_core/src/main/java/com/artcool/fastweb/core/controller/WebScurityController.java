package com.artcool.fastweb.core.controller;

/**
 * @author wuyibin
 * @date 2019/5/21
 */
public interface WebScurityController<T> {
    void check(T t);
}
