package com.artcool.fastweb.core.builder;

import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;

import com.artcool.fastweb.core.base.BaseIndicatorView;

/**
 * 进度条
 *
 * @author wuyibin
 * @date 2019/5/10
 */
public class IndicatorBuilder {

    private FastWebBuilder builder;

    public IndicatorBuilder(FastWebBuilder builder) {
        this.builder = builder;
    }

    public final CommonBuilder useDefaultIndicator() {
        builder.mDefaultIndicatorEnable = true;
        return new CommonBuilder(builder);
    }

    public final CommonBuilder useDefaultIndicator(int color) {
        builder.mDefaultIndicatorEnable = true;
        builder.mIndicatorColor = color;
        return new CommonBuilder(builder);
    }

    public final CommonBuilder useDefaultIndicator(@ColorInt int color, int heigh_dp) {
        builder.mIndicatorColor = color;
        builder.mHeight = heigh_dp;
        return new CommonBuilder(builder);
    }

    public final CommonBuilder closeIndicator() {
        builder.mDefaultIndicatorEnable = false;
        builder.mIndicatorColor = -1;
        return new CommonBuilder(builder);
    }

    public final CommonBuilder setCustomIndicator(@NonNull BaseIndicatorView view) {
        if (view != null) {
            builder.mDefaultIndicatorEnable = true;
            builder.mBaseIndicatorView = view;
            builder.mIsDefaultProgress = false;
        } else {
            builder.mDefaultIndicatorEnable = true;
            builder.mIsDefaultProgress = true;
        }
        return new CommonBuilder(builder);
    }


}
