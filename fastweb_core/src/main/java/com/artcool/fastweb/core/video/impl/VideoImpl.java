package com.artcool.fastweb.core.video.impl;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Build;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;

import com.artcool.fastweb.core.interceptor.EventInterceptor;
import com.artcool.fastweb.core.video.VideoListener;

import java.util.HashSet;
import java.util.Set;

/**
 * @author wuyibin
 * @date 2019/5/21
 */
public class VideoImpl implements VideoListener, EventInterceptor {

    private Activity mActivity;
    private WebView mWebView;
    private Set<Pair<Integer, Integer>> mFlags;
    private View mMovieView;
    private ViewGroup mMovieParentView;
    private WebChromeClient.CustomViewCallback mCallback;

    public VideoImpl(Activity activity, WebView webView) {
        this.mActivity = activity;
        this.mWebView = webView;
        mFlags = new HashSet<>();
    }

    @Override
    public boolean event() {
        if (isVideoState()) {
            onHideCustomView();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback) {
        Activity activity;
        if ((activity = this.mActivity) == null || mActivity.isFinishing()) {
            return;
        }

        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        Window window = activity.getWindow();
        Pair<Integer, Integer> mPair;
        /**
         * 保存当前屏幕状态
         */
        if ((window.getAttributes().flags & WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON) == 0) {
            mPair = new Pair<>(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, 0);
            window.setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            mFlags.add(mPair);
        }
        if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) && (window.getAttributes().flags & WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON) == 0) {
            mPair = new Pair<>(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED, 0);
            window.setFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED, WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
            mFlags.add(mPair);
        }
        if (mMovieView != null) {
            callback.onCustomViewHidden();
            return;
        }

        if (mWebView != null) {
            mWebView.setVisibility(View.GONE);
        }
        if (mMovieParentView == null) {
            FrameLayout mDecorView = (FrameLayout) activity.getWindow().getDecorView();
            mMovieParentView = new FrameLayout(activity);
            mMovieParentView.setBackgroundColor(Color.WHITE);
            mDecorView.addView(mMovieParentView);
        }
        this.mCallback = callback;
        mMovieParentView.addView(this.mMovieView = view);
        mMovieParentView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideCustomView() {
        if (mMovieView == null) {
            return;
        }
        if (mActivity != null && mActivity.getRequestedOrientation() != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
            mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        if (!mFlags.isEmpty()) {
            for (Pair<Integer, Integer> pair : mFlags) {
                mActivity.getWindow().setFlags(pair.second, pair.first);
            }
            mFlags.clear();
        }
        mMovieView.setVisibility(View.GONE);
        if (mMovieParentView != null && mMovieView != null) {
            mMovieParentView.removeView(mMovieView);
        }
        if (mMovieParentView != null) {
            mMovieParentView.setVisibility(View.GONE);
        }
        if (mCallback != null) {
            mCallback.onCustomViewHidden();
        }
        this.mMovieView = null;
        if (mWebView != null) {
            mWebView.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public boolean isVideoState() {
        return mMovieView != null;
    }
}
