package com.artcool.fastweb.core.js;

import java.util.Map;

/**
 *  对象注入接口
 * @author wuyibin
 * @date 2019/5/22
 */
public interface JsInterfaceHolder {

    JsInterfaceHolder addJavaObjects(Map<String,Object> maps);

    JsInterfaceHolder addJavaObject(String key,Object object);

    boolean checkObject(Object object);
}
