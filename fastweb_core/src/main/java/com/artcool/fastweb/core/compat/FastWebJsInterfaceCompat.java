package com.artcool.fastweb.core.compat;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.webkit.JavascriptInterface;

import com.artcool.fastweb.core.FastWeb;
import com.artcool.fastweb.core.utils.FastWebUtils;

import java.lang.ref.WeakReference;

/**
 *
 * @author wuyibin
 * @date 2019/5/21
 */
public class FastWebJsInterfaceCompat {
    private WeakReference<FastWeb> mReference;
    private WeakReference<Activity> mActivityReference;

    public FastWebJsInterfaceCompat(FastWeb fastWeb,Activity activity) {
        mReference = new WeakReference<>(fastWeb);
        mActivityReference = new WeakReference<>(activity);
    }

    @JavascriptInterface
    public void uploadFile() {
        uploadFile("*/*");
    }

    @JavascriptInterface
    public void uploadFile(String acceptType) {
        /*if (mActivityReference.get() != null && mReference.get() != null) {
            FastWebUtils.showFileChooserCompat(mActivityReference.get(),
                    mReference.get().getWebCreator().getWebView(),
                    null,
                    null,
                    mReference.get().getPermissionInterceptor(),
                    null,
                    acceptType,
                    new Handler.Callback() {
                        @Override
                        public boolean handleMessage(Message msg) {
                            if (mReference.get() != null) {
                                mReference.get().getJsAccessEntrace()
                                        .quickCallJs("uploadFileResult",
                                                msg.obj instanceof String ? (String) msg.obj : null);
                            }
                            return true;
                        }
                    }
            );
        }*/
    }
}
