package com.artcool.fastweb.core.js;

import android.webkit.ValueCallback;

/**
 *
 * @author wuyibin
 * @date 2019/5/21
 */
public interface JsAccessEntrance extends QuickCallJs{

    void callJs(String js, ValueCallback<String> callback);

    void callJs(String js);

}
