package com.artcool.fastweb.core.js.impl;

import android.os.Handler;
import android.os.Looper;
import android.webkit.ValueCallback;
import android.webkit.WebView;

import com.artcool.fastweb.core.js.BaseJsAccessEntrance;

/**
 *
 * @author wuyibin
 * @date 2019/5/21
 */
public class JsAccessEntranceImpl extends BaseJsAccessEntrance {

    private WebView mWebView;
    private Handler mHandler = new Handler(Looper.getMainLooper());

    private JsAccessEntranceImpl(WebView webView) {
        super(webView);
        this.mWebView = webView;
    }

    public static JsAccessEntranceImpl getInstance(WebView webView) {
        return new JsAccessEntranceImpl(webView);
    }

    private void safeCallJs(final String js, final ValueCallback callback) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                callJs(js,callback);
            }
        });
    }

    @Override
    public void callJs(String js, ValueCallback<String> callback) {
        if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
            safeCallJs(js,callback);
            return;
        }
        super.callJs(js,callback);
    }


}
