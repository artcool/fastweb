package com.artcool.fastweb.core.web.impl;

import android.os.Build;
import android.webkit.WebView;

import com.artcool.fastweb.core.utils.FastWebUtils;
import com.artcool.fastweb.core.web.WebLifeCycle;

/**
 * @author wuyibin
 * @date 2019/5/21
 */
public class DefaultWebLifeCycleImpl implements WebLifeCycle {
    private WebView mWebView;

    public DefaultWebLifeCycleImpl(WebView webView) {
        this.mWebView = webView;
    }

    @Override
    public void onResume() {
        if (mWebView != null) {
            if (Build.VERSION.SDK_INT >= 11) {
                mWebView.onResume();
            }
            mWebView.resumeTimers();
        }
    }

    @Override
    public void onPause() {
        if (mWebView != null) {
            if (Build.VERSION.SDK_INT >= 11) {
                mWebView.onPause();
            }
            mWebView.pauseTimers();
        }
    }

    @Override
    public void onDestroy() {
        if (mWebView != null) {
            mWebView.resumeTimers();
        }

        FastWebUtils.clearWebView(mWebView);
    }
}
