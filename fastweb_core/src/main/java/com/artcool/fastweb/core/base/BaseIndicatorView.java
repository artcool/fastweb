package com.artcool.fastweb.core.base;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.artcool.fastweb.core.base.listener.BaseIndicatorSpecListener;
import com.artcool.fastweb.core.base.listener.LayoutParamsOfferListener;

/**
 *
 * @author wuyibin
 * @date 2019/5/5
 */
public abstract class BaseIndicatorView extends FrameLayout implements BaseIndicatorSpecListener,LayoutParamsOfferListener {
    public BaseIndicatorView(@NonNull Context context) {
        super(context);
    }

    public BaseIndicatorView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseIndicatorView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void show() {


    }

    @Override
    public void hide() {

    }

    @Override
    public void reset() {

    }

    @Override
    public void setProgress(int newProgress) {

    }

}
