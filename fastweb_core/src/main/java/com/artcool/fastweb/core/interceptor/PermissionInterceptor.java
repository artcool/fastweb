package com.artcool.fastweb.core.interceptor;

/**
 * 权限拦截器
 *
 * @author wuyibin
 * @date 2019/5/5
 */
public interface PermissionInterceptor {

    /**
     * 过滤
     * @param url
     * @param permissions
     * @param action
     * @return
     */
    boolean intercept(String url, String[] permissions, String action);
}
