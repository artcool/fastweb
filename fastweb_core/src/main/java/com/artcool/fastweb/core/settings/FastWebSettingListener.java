package com.artcool.fastweb.core.settings;

import android.webkit.WebSettings;
import android.webkit.WebView;

/**
 *  web相关设置接口
 * @author wuyibin
 * @date 2019/5/5
 */
public interface FastWebSettingListener<T extends WebSettings> {

    /**
     * 设置web
     * @param webView
     * @return
     */
    FastWebSettingListener toSetting(WebView webView);

    /**
     * 获取web相关设置
     * @return
     */
    T getWebSettings();
}
