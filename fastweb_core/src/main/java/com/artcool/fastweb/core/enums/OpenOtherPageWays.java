package com.artcool.fastweb.core.enums;

import com.artcool.fastweb.core.client.DefaultWebClient;

/**
 * @author wuyibin
 * @date 2019/5/21
 */
public enum OpenOtherPageWays {

    /**
     * 直接打开跳转页
     */
    DERECT(DefaultWebClient.DERECT_OPEN_OTHER_PAGE),
    /**
     * 咨询用户是否打开
     */
    ASK(DefaultWebClient.ASK_USER_OPEN_OTHER_PAGE),
    /**
     * 禁止打开其他页面
     */
    DISALLOW(DefaultWebClient.DISALLOW_OPEN_OTHER_PAGE);

    public int code;

    OpenOtherPageWays(int code) {
        this.code = code;
    }
}
