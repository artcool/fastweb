package com.artcool.fastweb.core.permissions;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.artcool.fastweb.core.utils.FastWebUtils;

import java.io.File;
import java.util.ArrayList;

import static android.provider.MediaStore.EXTRA_OUTPUT;

/**
 * @author wuyibin
 * @date 2019/5/23
 */
public class ActionActivity extends Activity {

    public static final String KEY_ACTION = "KEY_ACTION";
    public static final String KEY_URI = "KEY_URI";
    public static final String KEY_FROM_INTENTION = "KEY_FROM_INTENTION";
    public static final String KEY_FILE_CHOOSER_INTENT = "KEY_FILE_CHOOSER_INTENT";
    public static final int REQUEST_CODE = 0x254;
    private Action mAction;
    private RationaleListener mRationaleListener;
    private PermissionListener mPermissionListener;
    private ChooserListener mChooserListener;
    private Uri mUri;

    public static void start(Activity activity, Action action) {
        Intent intent = new Intent(activity, ActionActivity.class);
        intent.putExtra(KEY_ACTION, action);
        activity.startActivity(intent);
    }

    public static void setRationaleListener(RationaleListener mRationaleListener) {
        mRationaleListener = mRationaleListener;
    }

    public static void setPermissionListener(PermissionListener mPermissionListener) {
        mPermissionListener = mPermissionListener;
    }

    public static void setChooserListener(ChooserListener mChooserListener) {
        mChooserListener = mChooserListener;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            return;
        }
        Intent intent = getIntent();
        mAction = intent.getParcelableExtra(KEY_ACTION);
        if (mAction == null) {
            cancelAction();
            finish();
            return;
        }
        if (mAction.getAction() == Action.ACTION_PERMISSION) {
            permission(mAction);
        } else if (mAction.getAction() == Action.ACTION_CAMERA) {
            realOpenCamera();
        } else {
            fetchFile();
        }
    }

    private void fetchFile() {
        if (mChooserListener == null) {
            finish();
        }
        realOpenFileChooser();
    }

    private void realOpenFileChooser() {
        try {
            if (mChooserListener == null) {
                finish();
                return;
            }
            Intent intent = getIntent().getParcelableExtra(KEY_FILE_CHOOSER_INTENT);
            if (intent == null) {
                cancelAction();
                return;
            }
            this.startActivityForResult(intent, REQUEST_CODE);
        } catch (Exception e) {
            chooserActionCallBack(-1, null);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            chooserActionCallBack(resultCode, mUri != null ? new Intent().putExtra(KEY_URI, mUri) : data);
        }
    }

    private void chooserActionCallBack(int resultCode, Intent data) {
        if (mChooserListener != null) {
            mChooserListener.onChoiceResult(REQUEST_CODE, resultCode, data);
            mChooserListener = null;
        }
        finish();
    }

    private void realOpenCamera() {
        if (mChooserListener == null) {
            finish();
        }
        File imageFile = FastWebUtils.createImageFile(this);
        if (imageFile == null) {
            mChooserListener.onChoiceResult(REQUEST_CODE, Activity.RESULT_CANCELED, null);
            mChooserListener = null;
            finish();
        }
        Intent intent = FastWebUtils.getIntentCaptureCompat(this, imageFile);
        mUri = intent.getParcelableExtra(EXTRA_OUTPUT);
        this.startActivityForResult(intent, REQUEST_CODE);
    }

    private void permission(Action action) {
        ArrayList<String> permissions = action.getPermissions();
        if (FastWebUtils.isEmptyCollection(permissions)) {
            mPermissionListener = null;
            mRationaleListener = null;
            finish();
            return;
        }
        if (mRationaleListener != null) {
            boolean rationale = false;
            for (String permission : permissions) {
                rationale = shouldShowRequestPermissionRationale(permission);
                if (rationale) {
                    break;
                }
            }
            mRationaleListener.onRationaleResult(rationale, new Bundle());
            mRationaleListener = null;
            finish();
            return;
        }
        if (mPermissionListener != null) {
            requestPermissions(permissions.toArray(new String[]{}), 1);
        }
    }

    private void cancelAction() {
        mRationaleListener = null;
        mPermissionListener = null;
        mChooserListener = null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public interface RationaleListener {
        void onRationaleResult(boolean showRationale, Bundle extras);
    }

    public interface PermissionListener {
        void onRequestPermissionsResult(@NonNull String[] permissions, @NonNull int[] grantResult, Bundle extras);
    }

    public interface ChooserListener {
        void onChoiceResult(int requestCode, int resultCode, Intent data);
    }
}
