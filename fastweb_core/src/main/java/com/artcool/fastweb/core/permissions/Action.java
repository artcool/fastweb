package com.artcool.fastweb.core.permissions;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author wuyibin
 * @date 2019/5/23
 */
public class Action implements Parcelable {

    public transient static final int ACTION_PERMISSION = 1;
    public transient static final int ACTION_FILE = 2;
    public transient static final int ACTION_CAMERA = 3;
    public static final Creator<Action> CREATOR = new Creator<Action>() {
        @Override
        public Action createFromParcel(Parcel in) {
            return new Action(in);
        }

        @Override
        public Action[] newArray(int size) {
            return new Action[size];
        }
    };
    private ArrayList<String> mPermissions = new ArrayList<>();
    private int mAction;
    private int mFromIntention;

    public Action() {
    }

    protected Action(Parcel in) {
        mPermissions = in.createStringArrayList();
        mAction = in.readInt();
        mFromIntention = in.readInt();
    }

    public ArrayList<String> getPermissions() {
        return mPermissions;
    }

    public void setPermissions(ArrayList<String> mPermissions) {
        this.mPermissions = mPermissions;
    }

    public void setPermissions(String[] mPermissions) {
        this.mPermissions = new ArrayList<>(Arrays.asList(mPermissions));
    }

    public int getAction() {
        return mAction;
    }

    public void setAction(int mAction) {
        this.mAction = mAction;
    }

    public int getFromIntention() {
        return mFromIntention;
    }

    public void setFromIntention(int mFromIntention) {
        this.mFromIntention = mFromIntention;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(mPermissions);
        dest.writeInt(mAction);
        dest.writeInt(mFromIntention);
    }

    public static Action createPermissionsAction(String[] permissions) {
        Action action = new Action();
        action.setAction(Action.ACTION_PERMISSION);
        List<String> list = Arrays.asList(permissions);
        action.setPermissions(new ArrayList<String>(list));
        return action;
    }
}
