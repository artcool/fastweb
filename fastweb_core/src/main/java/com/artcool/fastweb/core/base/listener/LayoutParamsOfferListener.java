package com.artcool.fastweb.core.base.listener;

import android.widget.FrameLayout;

/**
 *
 * @author wuyibin
 * @date 2019/5/5
 */
public interface LayoutParamsOfferListener<T extends FrameLayout.LayoutParams> {


    T offerLayoutParams();

}
