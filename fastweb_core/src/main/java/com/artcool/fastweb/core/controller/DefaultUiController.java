package com.artcool.fastweb.core.controller;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebView;
import android.widget.EditText;

import com.artcool.fastweb.core.FastWeb;
import com.artcool.fastweb.core.R;
import com.artcool.fastweb.core.layout.WebParentLayout;
import com.artcool.fastweb.core.utils.FastWebUtils;

/**
 *
 * @author wuyibin
 * @date 2019/5/5
 */
public class DefaultUiController extends AbsFastWebUIController {

    private AlertDialog mAlertDialog;
    protected AlertDialog mConfirmDialog;
    private JsPromptResult mJsPromptResult = null;
    private JsResult mJsResult = null;
    private AlertDialog mPromptDialog = null;
    private Activity mActivity;
    private WebParentLayout mWebParentLayout;
    private AlertDialog mAskOpenOtherAppDialog = null;
    private ProgressDialog mProgressDialog;
    private Resources mResources = null;

    @Override
    protected void bindSupportWebParent(WebParentLayout webParentLayout, Activity activity) {
        this.mActivity = activity;
        this.mWebParentLayout = webParentLayout;
        mResources = this.mActivity.getResources();
    }

    @Override
    public void onJsAlert(WebView webView, String url, String message) {
        FastWebUtils.toastShowShort(webView.getContext().getApplicationContext(),message);
    }

    @Override
    public void onOpenOtherPage(WebView webView, String url, final Handler.Callback callback) {
        if (mAskOpenOtherAppDialog == null) {
            mAskOpenOtherAppDialog = new AlertDialog.Builder(mActivity)
                    .setMessage(mResources.getString(R.string.fast_web_leave_app_and_go_other_page,FastWebUtils.getApplicationName(mActivity)))
                    .setTitle(mResources.getString(R.string.fast_web_tips))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (callback != null) {
                                callback.handleMessage(Message.obtain(null,-1));
                            }
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (callback != null) {
                                callback.handleMessage(Message.obtain(null,1));
                            }
                        }
                    })
                    .create();
        }
        mAskOpenOtherAppDialog.show();
    }

    @Override
    public void onJsConfirm(WebView webView, String url, String message, JsResult result) {
        onJsConfirmInternal(message,result);
    }

    @Override
    public void onSelectItemsPrompt(WebView webView, String url, String[] ways, Handler.Callback callback) {
            showSelectInternal(ways,callback);
    }

    private void showSelectInternal(String[] ways, final Handler.Callback callback) {
        mAlertDialog = new AlertDialog.Builder(mActivity)
                .setSingleChoiceItems(ways, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (callback != null) {
                            Message mMessage = Message.obtain();
                            mMessage.what = which;
                            callback.handleMessage(mMessage);
                        }
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        dialog.dismiss();
                        if (callback != null) {
                            callback.handleMessage(Message.obtain(null,-1));
                        }
                    }
                })
                .create();
        mAlertDialog.show();
    }

    @Override
    public void onForceDownloadAlert(String url, Handler.Callback callback) {
        onForceDownloadAlertInternal(callback);
    }

    private void onForceDownloadAlertInternal(final Handler.Callback callback) {
        Activity mActivity;
        if ((mActivity = this.mActivity) == null || mActivity.isFinishing()) {
            return;
        }
        AlertDialog mAlertDialog = null;
        mAlertDialog = new AlertDialog.Builder(mActivity)
                .setTitle(mResources.getString(R.string.fast_web_tips))
                .setMessage(mResources.getString(R.string.fast_web_comblow))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                        if (callback != null) {
                            callback.handleMessage(Message.obtain());
                        }
                    }
                })
                .create();
        mAlertDialog.show();
    }

    @Override
    public void onJsPrompt(WebView webView, String url, String message, String defaultValue, JsPromptResult result) {
        onJsPromptInternal(message,defaultValue,result);
    }

    private void onJsPromptInternal(String message, String defaultValue, JsPromptResult result) {
        Activity activity = this.mActivity;
        if (activity == null || activity.isFinishing()) {
            return;
        }
        if (mProgressDialog == null) {
            final EditText et = new EditText(mActivity);
            et.setText(defaultValue);
            mPromptDialog = new AlertDialog.Builder(mActivity)
                    .setView(et)
                    .setTitle(message)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            toDismissDialog(mPromptDialog);
                            if (mJsPromptResult != null) {
                                mJsPromptResult.confirm(et.getText().toString());
                            }
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            toDismissDialog(mPromptDialog);
                            toCancelJsResult(mJsPromptResult);
                        }
                    })
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            dialog.dismiss();
                            toCancelJsResult(mJsPromptResult);
                        }
                    })
                    .create();
        }
        this.mJsPromptResult = result;
        mPromptDialog.show();
    }

    @Override
    public void onShowErrorPage(WebView webView, int errCode, String description, String failingUrl) {
        if (mWebParentLayout != null) {
            mWebParentLayout.showErrorPage();
        }
    }

    @Override
    public void onHideErrorPage() {
        if (mWebParentLayout != null) {
            mWebParentLayout.hideErrorLayout();
        }
    }

    @Override
    public void onLoading(String msg) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(mActivity);
        }
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }

    @Override
    public void onCancelLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
        mProgressDialog = null;
    }

    @Override
    public void onShowMessage(String message, String intent) {
        if (!TextUtils.isEmpty(intent) && intent.contains("performDownload")) {
            return;
        }
        FastWebUtils.toastShowShort(mActivity.getApplicationContext(),message);
    }

    @Override
    public void onPermissionDeny(String[] permissions, String permissionType, String action) {
        FastWebUtils.toastShowShort(mActivity.getApplicationContext(),"您拒绝了赋予权限");
    }

    private void onJsConfirmInternal(String message, JsResult result) {
        Activity mActivityt = this.mActivity;
        if (mActivityt == null || mActivityt.isFinishing()) {
            toCancelJsResult(result);
            return;
        }

        if (mConfirmDialog == null) {
            mConfirmDialog = new AlertDialog.Builder(mActivityt)
                    .setMessage(message)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            toDismissDialog(mConfirmDialog);
                            if (mJsResult != null) {
                                mJsResult.confirm();
                            }
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            toDismissDialog(mConfirmDialog);
                            toCancelJsResult(mJsResult);
                        }
                    })
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            dialog.dismiss();
                            toCancelJsResult(mJsResult);
                        }
                    })
                    .create();
        }
        mConfirmDialog.setMessage(message);
        this.mJsResult = result;
        mConfirmDialog.show();
    }

    private void toCancelJsResult(JsResult result) {
        if (result != null) {
            result.cancel();
        }
    }
}
