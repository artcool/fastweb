package com.artcool.fastweb.core.video;

import android.view.View;
import android.webkit.WebChromeClient;

/**
 *
 * @author wuyibin
 * @date 2019/5/21
 */
public interface VideoListener {

    /**
     * 显示自定义的视频view
     * @param view
     * @param callback
     */
    void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback);

    /**
     * 隐藏view
     */
    void onHideCustomView();

    /**
     * 判断是否是视频播放状态
     */
    boolean isVideoState();
}
