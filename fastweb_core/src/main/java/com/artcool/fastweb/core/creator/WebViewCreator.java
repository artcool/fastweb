package com.artcool.fastweb.core.creator;

import android.webkit.WebView;
import android.widget.FrameLayout;

import com.artcool.fastweb.core.creator.listener.WebIndicatorListener;

/**
 *
 * @author wuyibin
 * @date 2019/5/5
 */
public interface WebViewCreator extends WebIndicatorListener {

    /**
     * 建造webview
     */
    WebViewCreator create();

    /**
     * 获取webview
     * @return
     */
    WebView getWebView();

    /**
     * 获取webview父布局
     * @return
     */
    FrameLayout getWebParenLayout();

}
