package com.artcool.fastweb.core.settings;

import android.os.Build;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.artcool.fastweb.core.FastWeb;
import com.artcool.fastweb.core.config.FastWebConfig;
import com.artcool.fastweb.core.settings.impl.FastWebSettingsImpl;
import com.artcool.fastweb.core.utils.FastWebUtils;
import com.artcool.fastweb.core.web.WebListenerManager;

/**
 *
 * @author wuyibin
 * @date 2019/5/21
 */
public abstract class AbsFastWebSettings implements FastWebSettingListener, WebListenerManager {

    public static final String USERAGENT_UC = "UCBrowser/11.6.4.950";
    public static final String USERAGENT_QQ = "MQQBrowser/8.0";
    public static final String USERAGENT_FASTWEB = FastWebConfig.FASTWEB_VERSION;

    private WebSettings mSettings;
    private FastWeb mFastWeb;

    public static AbsFastWebSettings getInstance() {
        return new FastWebSettingsImpl();
    }

    public AbsFastWebSettings(){}

    public final void bindFastWeb(FastWeb fastWeb){
        this.mFastWeb = fastWeb;
        this.bindFastWebSupport(fastWeb);
    }

    protected abstract void bindFastWebSupport(FastWeb fastWeb);


    @Override
    public FastWebSettingListener toSetting(WebView webView) {
        settings(webView);
        return this;
    }

    private void settings(WebView webView){
        mSettings = webView.getSettings();
        mSettings.setJavaScriptEnabled(true);
        mSettings.setSupportZoom(true);
        mSettings.setBuiltInZoomControls(false);
        mSettings.setSavePassword(false);
        if (FastWebUtils.checkNetwork(webView.getContext())) {
            mSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        } else {
            //没网，从本地加载，即离线加载
            mSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //适配5.0不允许http和HTTPS混合使用情况
            mSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            webView.setLayerType(View.LAYER_TYPE_HARDWARE,null);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webView.setLayerType(View.LAYER_TYPE_HARDWARE,null);
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE,null);
        }
        mSettings.setTextZoom(100);
        mSettings.setDatabaseEnabled(true);
        mSettings.setAppCacheEnabled(true);
        mSettings.setLoadsImagesAutomatically(true);
        mSettings.setSupportMultipleWindows(false);
        //是否阻塞加载网络图片，协议http or https
        mSettings.setBlockNetworkImage(false);
        //允许加载本地HTML文件
        mSettings.setAllowFileAccess(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mSettings.setAllowFileAccessFromFileURLs(false);
            mSettings.setAllowUniversalAccessFromFileURLs(false);
        }
        mSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        } else {
            mSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        }
        mSettings.setLoadWithOverviewMode(false);
        mSettings.setUseWideViewPort(false);
        mSettings.setDomStorageEnabled(true);
        mSettings.setNeedInitialFocus(true);
        mSettings.setDefaultTextEncodingName("UTF-8");
        mSettings.setDefaultFontSize(16);
        mSettings.setMinimumFontSize(12);
        mSettings.setGeolocationEnabled(true);
        String dir = FastWebConfig.getCachePath(webView.getContext());
        mSettings.setGeolocationDatabasePath(dir);
        mSettings.setDatabasePath(dir);
        mSettings.setAppCachePath(dir);
        mSettings.setAppCacheMaxSize(Long.MAX_VALUE);
        mSettings.setUserAgentString(getWebSettings().getUserAgentString().concat(USERAGENT_FASTWEB).concat(USERAGENT_UC));

    }

    @Override
    public WebSettings getWebSettings() {
        return mSettings;
    }

    @Override
    public WebListenerManager setWebChromeClient(WebView webView, WebChromeClient webChromeClient) {
        webView.setWebChromeClient(webChromeClient);
        return this;
    }

    @Override
    public WebListenerManager setWebViewClient(WebView webView, WebViewClient webViewClient) {
        webView.setWebViewClient(webViewClient);
        return this;
    }

    @Override
    public WebListenerManager setDownloader(WebView webView, DownloadListener downloadListener) {
        webView.setDownloadListener(downloadListener);
        return this;
    }
}
