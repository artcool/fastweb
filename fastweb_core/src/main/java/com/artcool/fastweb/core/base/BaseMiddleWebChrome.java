package com.artcool.fastweb.core.base;

import android.webkit.WebChromeClient;

import com.artcool.fastweb.core.client.delegate.FastWebChromeClientDelegate;

/**
 *
 * @author wuyibin
 * @date 2019/5/10
 */
public class BaseMiddleWebChrome extends FastWebChromeClientDelegate {

    private BaseMiddleWebChrome mBaseMiddleWebChrome;

    protected BaseMiddleWebChrome(WebChromeClient mDelegate) {
        super(mDelegate);
    }
    protected BaseMiddleWebChrome(){
        super(null);
    }

    @Override
    public void setDelegate(WebChromeClient delegate) {
        super.setDelegate(delegate);
    }

    public final BaseMiddleWebChrome enqueue(BaseMiddleWebChrome baseMiddleWebChrome) {
        setDelegate(baseMiddleWebChrome);
        this.mBaseMiddleWebChrome = baseMiddleWebChrome;
        return mBaseMiddleWebChrome;
    }

    public final BaseMiddleWebChrome next() {
        return mBaseMiddleWebChrome;
    }
}
