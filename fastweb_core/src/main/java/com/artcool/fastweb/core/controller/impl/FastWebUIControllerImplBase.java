package com.artcool.fastweb.core.controller.impl;

import android.app.Activity;
import android.os.Handler;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebView;

import com.artcool.fastweb.core.controller.AbsFastWebUIController;
import com.artcool.fastweb.core.layout.WebParentLayout;

/**
 *
 * @author wuyibin
 * @date 2019/5/10
 */
public class FastWebUIControllerImplBase extends AbsFastWebUIController {
    @Override
    protected void bindSupportWebParent(WebParentLayout webParentLayout, Activity activity) {
        getDelegate().bindWebParent(webParentLayout, activity);
    }

    @Override
    public void onJsAlert(WebView webView, String url, String message) {
        getDelegate().onJsAlert(webView, url, message);
    }

    @Override
    public void onOpenOtherPage(WebView webView, String url, Handler.Callback callback) {
        getDelegate().onOpenOtherPage(webView, url, callback);
    }

    @Override
    public void onJsConfirm(WebView webView, String url, String message, JsResult result) {
        getDelegate().onJsConfirm(webView, url, message, result);
    }

    @Override
    public void onSelectItemsPrompt(WebView webView, String url, String[] ways, Handler.Callback callback) {
        getDelegate().onSelectItemsPrompt(webView, url, ways, callback);
    }

    @Override
    public void onForceDownloadAlert(String url, Handler.Callback callback) {
        getDelegate().onForceDownloadAlert(url, callback);
    }

    @Override
    public void onJsPrompt(WebView webView, String url, String message, String defaultValue, JsPromptResult result) {
        getDelegate().onJsPrompt(webView, url, message, defaultValue, result);
    }

    @Override
    public void onShowErrorPage(WebView webView, int errCode, String description, String failingUrl) {
        getDelegate().onShowErrorPage(webView, errCode, description, failingUrl);
    }

    @Override
    public void onHideErrorPage() {
        getDelegate().onHideErrorPage();
    }

    @Override
    public void onLoading(String msg) {
        getDelegate().onLoading(msg);
    }

    @Override
    public void onCancelLoading() {
        getDelegate().onCancelLoading();
    }

    @Override
    public void onShowMessage(String message, String intent) {
        getDelegate().onShowMessage(message, intent);
    }

    @Override
    public void onPermissionDeny(String[] permissions, String permissionType, String action) {
        getDelegate().onPermissionDeny(permissions, permissionType, action);
    }

    public static AbsFastWebUIController build() {
        return new FastWebUIControllerImplBase();
    }
}
