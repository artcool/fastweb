package com.artcool.fastweb.core.js;

import android.os.Build;
import android.webkit.ValueCallback;
import android.webkit.WebView;

import com.artcool.fastweb.core.utils.FastWebUtils;

/**
 *
 * @author wuyibin
 * @date 2019/5/21
 */
public abstract class BaseJsAccessEntrance implements JsAccessEntrance {
    private WebView mWebView;

    public BaseJsAccessEntrance(WebView webView) {
        this.mWebView = webView;
    }


    @Override
    public void callJs(String js) {
        callJs(js,null);
    }

    @Override
    public void callJs(String js, ValueCallback<String> callback) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            evaluateJs(js, callback);
        } else {
            loadJs(js);
        }
    }

    @Override
    public void quickCallJs(String methodName) {
        quickCallJs(methodName,new String[]{});
    }

    @Override
    public void quickCallJs(String methodName, String... params) {
        quickCallJs(methodName,null,params);
    }

    @Override
    public void quickCallJs(String methodName, ValueCallback<String> callback, String... params) {
        StringBuilder sb = new StringBuilder();
        sb.append("javascript:" + methodName);
        if (params == null || params.length == 0) {
            sb.append("()");
        } else {
            sb.append("(").append(concat(params)).append(")");
        }
        callJs(sb.toString(),callback);
    }

    private String concat(String... params) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < params.length; i++) {
            String param = params[i];
            if (!FastWebUtils.isJson(param)) {
                builder.append("\"").append(param).append("\"");
            } else {
                builder.append(param);
            }
            if (i != params.length -1) {
                builder.append(" , ");
            }
        }
        return builder.toString();
    }

    private void loadJs(String js) {
        mWebView.loadUrl(js);
    }

    private void evaluateJs(String js, final ValueCallback<String> callback) {
        mWebView.evaluateJavascript(js, new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String value) {
                if (callback != null) {
                    callback.onReceiveValue(value);
                }
            }
        });
    }
}
