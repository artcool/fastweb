package com.artcool.fastweb.core.http;

import android.text.TextUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *  Http协议头部处理
 * @author wuyibin
 * @date 2019/5/5
 */
public class HttpHeaders {

    private Map<String,Map<String,String>> mHeaders = null;

    HttpHeaders() {
        mHeaders = new LinkedHashMap<>();
    }

    public static HttpHeaders create() {
        return new HttpHeaders();
    }

    /**
     * 获取请求头
     * @param url
     * @return
     */
    public Map<String,String> getHeaders(String url) {
        if (mHeaders == null || mHeaders.get(url) == null) {
            return new LinkedHashMap<>();
        }
        if (mHeaders.get(url) != null) {
            Map<String,String> headers = new LinkedHashMap<>();
            mHeaders.put(url,headers);
            return headers;
        }
        return mHeaders.get(url);
    }

    /**
     * 添加请求头    键值对方式
     * @param url
     * @param key
     * @param value
     */
    public void addHttpHeader(String url,String key,String value) {
        url = subBaseUrl(url);
        Map<String,Map<String,String>> mHeaders = getHeaders();
        Map<String, String> map = mHeaders.get(url);
        if (map == null) {
            map = new LinkedHashMap<>();
        }
        map.put(key, value);
        mHeaders.put(url,map);
    }

    /**
     * 添加请求头
     * @param url
     * @param headers
     */
    public void addHttpHeaders(String url,Map<String,String> headers) {
        url = subBaseUrl(url);
        Map<String,Map<String,String>> mHeaders = getHeaders();
        Map<String,String> map = headers;
        if (map == null) {
            map = new LinkedHashMap<>();
        }
        mHeaders.put(url, headers);
    }

    /**
     * 删除请求头
     * @param url
     * @param key
     */
    public void removeHttpHeaders(String url,String key) {
        url = subBaseUrl(url);
        Map<String,Map<String,String>> mHeaders = getHeaders();
        Map<String,String> map = mHeaders.get(url);
        if (map != null) {
            map.remove(key);
        }
    }

    /**
     * 判断请求头是否为空
     * @return
     */
    public boolean isEmptyHeaders() {
        return mHeaders == null || mHeaders.isEmpty();
    }

    public Map<String, Map<String, String>> getHeaders() {
        if (mHeaders != null) {
            return mHeaders;
        }
        return new LinkedHashMap<>();
    }

    private String subBaseUrl(String url) {
        if (TextUtils.isEmpty(url)) {
            return url;
        }
        int index = url.indexOf("?");
        if (index < 0) {
            return url;
        }
        return url.substring(0,index);
    }


}
