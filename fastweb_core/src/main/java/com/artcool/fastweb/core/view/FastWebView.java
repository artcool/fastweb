package com.artcool.fastweb.core.view;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

/**
 * Created by wuyibin on 2019/5/14.
 */
public class FastWebView extends WebView {
    public FastWebView(Context context) {
        super(context);
    }

    public FastWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

}
