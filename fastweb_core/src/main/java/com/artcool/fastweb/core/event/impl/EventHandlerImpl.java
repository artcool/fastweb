package com.artcool.fastweb.core.event.impl;

import android.view.KeyEvent;
import android.webkit.WebView;

import com.artcool.fastweb.core.event.EventHandlerListener;
import com.artcool.fastweb.core.interceptor.EventInterceptor;

/**
 *  事件拦截器实现类
 * @author wuyibin
 * @date 2019/5/21
 */
public class EventHandlerImpl implements EventHandlerListener {

    private WebView mWebView;
    private EventInterceptor mEventInterceptor;

    public EventHandlerImpl(WebView webView, EventInterceptor interceptor) {
        this.mWebView = webView;
        this.mEventInterceptor = interceptor;
    }

    public static final EventHandlerImpl getInstance(WebView webView, EventInterceptor interceptor) {
        return new EventHandlerImpl(webView, interceptor);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return back();
        }
        return false;
    }

    @Override
    public boolean back() {
        if (mEventInterceptor != null && mEventInterceptor.event()) {
            return true;
        }
        if (mWebView != null && mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }
        return false;
    }
}
