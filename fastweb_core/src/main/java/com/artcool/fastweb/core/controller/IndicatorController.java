package com.artcool.fastweb.core.controller;

import android.webkit.WebView;

import com.artcool.fastweb.core.base.listener.BaseIndicatorSpecListener;

/**
 * @author wuyibin
 * @date 2019/5/5
 */
public interface IndicatorController {

    /**
     * 进度条
     * @param webView
     * @param newProgress
     */
    void progress(WebView webView, int newProgress);

    BaseIndicatorSpecListener offerIndicator();

    /**
     * 显示进度条
     */
    void showIndicator();

    /**
     * 设置进度条
     * @param newProgress
     */
    void setProgress(int newProgress);

    /**
     * 完成
     */
    void finish();
}
