package com.artcool.fastweb.core.interceptor;

/**
 *  事件拦截器
 * @author wuyibin
 * @date 2019/5/21
 */
public interface EventInterceptor {
    boolean event();
}
