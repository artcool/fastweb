package com.artcool.fastweb.core.settings.impl;

import android.webkit.DownloadListener;
import android.webkit.WebView;

import com.artcool.fastweb.core.FastWeb;
import com.artcool.fastweb.core.settings.AbsFastWebSettings;
import com.artcool.fastweb.core.web.WebListenerManager;

/**
 * @author wuyibin
 * @date 2019/5/21
 */
public class FastWebSettingsImpl extends AbsFastWebSettings {

    private FastWeb mFastWeb;

    @Override
    protected void bindFastWebSupport(FastWeb fastWeb) {
        this.mFastWeb = fastWeb;
    }

}
