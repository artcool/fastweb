package com.artcool.fastweb.core.builder;

import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebView;

import com.artcool.fastweb.core.base.BaseMiddleWebChrome;
import com.artcool.fastweb.core.base.BaseMiddlewareWebClient;
import com.artcool.fastweb.core.client.DefaultWebClient;
import com.artcool.fastweb.core.client.FastWebViewClient;
import com.artcool.fastweb.core.controller.impl.FastWebUIControllerImplBase;
import com.artcool.fastweb.core.enums.OpenOtherPageWays;
import com.artcool.fastweb.core.event.EventHandlerListener;
import com.artcool.fastweb.core.interceptor.PermissionInterceptor;
import com.artcool.fastweb.core.layout.listener.WebLayoutListener;
import com.artcool.fastweb.core.security.SecurityType;
import com.artcool.fastweb.core.settings.FastWebSettingListener;
import com.artcool.fastweb.core.web.PreFastWeb;

import java.util.Map;

/**
 * Created by wuyibin on 2019/5/10.
 */
public class CommonBuilder {
    private FastWebBuilder builder;

    public CommonBuilder(FastWebBuilder builder) {
        this.builder = builder;
    }

    public CommonBuilder setEventHandler(@NonNull EventHandlerListener listener) {
        builder.mEventHandlerListener = listener;
        return this;
    }

    public CommonBuilder closeWebViewClientHelper() {
        builder.mWebClientHelper = false;
        return this;
    }

    public CommonBuilder setWebChromeClient(@NonNull FastWebViewClient client) {
        builder.mFastWebViewClient = client;
        return this;
    }

    public CommonBuilder useMiddleWebClient(@NonNull BaseMiddlewareWebClient client) {
        if (client == null) {
            return this;
        }
        if (builder.mBaseMiddlewareWebClientHeader == null) {
            this.builder.mBaseMiddlewareWebClientHeader = this.builder.mBaseMiddlewareWebClientTail = client;
        } else {
            this.builder.mBaseMiddlewareWebClientTail.enqueue(client);
            this.builder.mBaseMiddlewareWebClientTail = client;
        }
        return this;
    }

    public CommonBuilder useMiddleWebChrome(@NonNull BaseMiddleWebChrome chrome) {
        if (chrome == null) {
            return this;
        }
        if (this.builder.mBaseMiddleWebChromeHeader == null) {
            this.builder.mBaseMiddleWebChromeHeader = this.builder.mBaseMiddleWebChromeTail = chrome;
        } else {
            this.builder.mBaseMiddleWebChromeTail.enqueue(chrome);
            this.builder.mBaseMiddleWebChromeTail = chrome;
        }
        return this;
    }

    public CommonBuilder setShowErrorView(@NonNull View view) {
        this.builder.mErrorView = view;
        return this;
    }

    public CommonBuilder setShowErrorView(@LayoutRes int errorLayout, @IdRes int clickViewId) {
        this.builder.mErrorLayout = errorLayout;
        this.builder.mReloadId = clickViewId;
        return this;
    }

    public CommonBuilder setFastWebSetting(@Nullable FastWebSettingListener listener) {
        this.builder.mFastWebSettingListener = listener;
        return this;
    }

    public CommonBuilder addJavascriptInterface(@NonNull String name,@NonNull Object o) {
        this.builder.addJavaObject(name, o);
        return this;
    }

    public CommonBuilder setSecurityType(@NonNull SecurityType type) {
        this.builder.mSecurityType = type;
        return this;
    }

    public CommonBuilder setWebView(@NonNull WebView webView) {
        this.builder.mWebView = webView;
        return this;
    }

    public CommonBuilder setWebLayout(@NonNull WebLayoutListener listener) {
        this.builder.mWebLayoutListener = listener;
        return this;
    }

    public CommonBuilder addHttpHeader(String baseUrl,String key,String value) {
        this.builder.addHeader(baseUrl,key,value);
        return this;
    }

    public CommonBuilder addHttpHeader(String baseUrl, Map<String,String> headers) {
        this.builder.addHeader(baseUrl, headers);
        return this;
    }

    public CommonBuilder setPermissionInterceptor(@Nullable PermissionInterceptor interceptor) {
        this.builder.mPermissionInterceptor = interceptor;
        return this;
    }

    public CommonBuilder setFastWebUIController(@Nullable FastWebUIControllerImplBase controller) {
        this.builder.mWebUIController = controller;
        return this;
    }

    public CommonBuilder setOpenOtherPageWays(@Nullable OpenOtherPageWays ways) {
        this.builder.mOpenOtherPage = ways;
        return this;
    }

    public CommonBuilder interceptUnkownUrl() {
        this.builder.isInterceptUnkownUrl = true;
        return this;
    }

    public PreFastWeb createFastWeb() {
        return this.builder.buildFastWeb();
    }
}
