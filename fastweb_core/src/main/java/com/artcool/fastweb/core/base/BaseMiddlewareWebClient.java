package com.artcool.fastweb.core.base;

import android.webkit.WebViewClient;

import com.artcool.fastweb.core.client.delegate.FastWebViewClientDelegate;

/**
 * @author wuyibin
 * @date 2019/5/6
 */
public class BaseMiddlewareWebClient extends FastWebViewClientDelegate {
    private BaseMiddlewareWebClient mBaseMiddlewareWebClient;

    BaseMiddlewareWebClient(BaseMiddlewareWebClient client) {
        super(client);
        this.mBaseMiddlewareWebClient = client;
    }

    protected BaseMiddlewareWebClient(WebViewClient client) {
        super(client);
    }

    protected BaseMiddlewareWebClient() {
        super(null);
    }

    final BaseMiddlewareWebClient next() {
        return this.mBaseMiddlewareWebClient;
    }

    @Override
    public void setDelegate(WebViewClient delegate) {
        super.setDelegate(delegate);
    }

    public final BaseMiddlewareWebClient enqueue(BaseMiddlewareWebClient client) {
        setDelegate(client);
        this.mBaseMiddlewareWebClient = client;
        return client;
    }
}
