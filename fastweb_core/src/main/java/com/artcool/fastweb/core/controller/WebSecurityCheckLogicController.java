package com.artcool.fastweb.core.controller;

import android.webkit.WebView;

import com.artcool.fastweb.core.FastWeb;
import com.artcool.fastweb.core.security.SecurityType;

import java.util.LinkedHashMap;

/**
 *
 * @author wuyibin
 * @date 2019/5/21
 */
public interface WebSecurityCheckLogicController {

    void dealHoneyComb(WebView webView);

    void dealJsInterface(LinkedHashMap<String,Object> objects, SecurityType securityType);

}
