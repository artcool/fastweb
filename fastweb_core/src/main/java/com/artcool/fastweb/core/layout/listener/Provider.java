package com.artcool.fastweb.core.layout.listener;

/**
 *
 * @author wuyibin
 * @date 2019/5/5
 */
public interface Provider<T> {
    T provide();
}
