package com.artcool.fastweb.core.client;

import android.webkit.WebViewClient;

import com.artcool.fastweb.core.client.delegate.FastWebViewClientDelegate;

/**
 * Created by wuyibin on 2019/5/5.
 */
public class FastWebViewClient extends FastWebViewClientDelegate {

    public FastWebViewClient() {
        super(null);
    }
}
