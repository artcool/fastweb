package com.artcool.fastweb.core.web;

import android.support.annotation.Nullable;

import com.artcool.fastweb.core.FastWeb;

/**
 * @author wuyibin
 * @date 2019/5/10
 */
public class PreFastWeb {

    private FastWeb mFastWeb;
    private boolean isReady = false;

    public PreFastWeb(FastWeb web) {
        this.mFastWeb = web;
    }

    public PreFastWeb ready() {
        if (!isReady) {
            mFastWeb.ready();
            isReady = true;
        }
        return this;
    }

    public FastWeb get() {
        ready();
        return mFastWeb;
    }

    public FastWeb go(@Nullable String url) {
        if (!isReady) {
            ready();
        }
        return mFastWeb.go(url);
    }
}
