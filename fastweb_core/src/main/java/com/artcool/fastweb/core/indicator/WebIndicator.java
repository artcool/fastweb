package com.artcool.fastweb.core.indicator;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;

import com.artcool.fastweb.core.base.BaseIndicatorView;
import com.artcool.fastweb.core.base.listener.BaseIndicatorSpecListener;
import com.artcool.fastweb.core.utils.FastWebUtils;

/**
 * 自定义进度条View
 *
 * @author wuyibin
 * @date 2019/5/14
 */
public class WebIndicator extends BaseIndicatorView implements BaseIndicatorSpecListener {

    /**
     * 默认匀速动画最大的时长
     */
    public static final int MAX_UNIFORM_SPEED_DURATION = 8 * 1000;
    /**
     * 默认加速后减速动画最大时长
     */
    public static final int MAX_DECELERATE_SPEED_DURATION = 450;
    /**
     * 结束动画时长，fade out
     */
    public static final int DO_END_ANIMATION_DURATION = 600;
    public static final int UN_START = 0;
    public static final int STARTED = 1;
    public static final int FINISH = 2;
    /**
     * 默认的高度
     */
    public static int WEB_INDICATOR_DEFAULT_HEIGHT = 3;
    /**
     * 当前匀速动画最大的时长
     */
    private static int CURRENT_MAX_UNIFORM_SPEED_DURATION = MAX_UNIFORM_SPEED_DURATION;
    /**
     * 当前加速后减速动画最大时长
     */
    private static int CURRENT_MAX_DECELERATE_SPEED_DURATION = MAX_DECELERATE_SPEED_DURATION;
    /**
     * 进度条眼色
     */
    private int mColor;
    /**
     * 进度条画笔
     */
    private Paint mPaint;
    /**
     * 进度条动画
     */
    private Animator mAnimator;
    /**
     * 控件的宽度
     */
    private int mTargetWidth = 0;
    /**
     * 标志当前进度条的状态
     */
    private int TAG = 0;
    private float mTarger = 0f;
    private float mCurrentProgress = 0f;
    private ValueAnimator.AnimatorUpdateListener mAnimatorUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            float t = (float) animation.getAnimatedValue();
            WebIndicator.this.mCurrentProgress = t;
            WebIndicator.this.invalidate();
        }
    };
    private AnimatorListenerAdapter mAnimatorListenerAdapter = new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
            doEnd();
        }
    };

    public WebIndicator(@NonNull Context context) {
        super(context);
        init(context,null,-1);
    }

    public WebIndicator(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs,-1);
    }

    public WebIndicator(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        mPaint = new Paint();
        mColor = Color.parseColor("#1aad17");
        /**
         * 抗锯齿
         */
        mPaint.setAntiAlias(true);
        mPaint.setColor(mColor);
        mPaint.setDither(true);
        mPaint.setStrokeCap(Paint.Cap.SQUARE);
        mTargetWidth = context.getResources().getDisplayMetrics().widthPixels;
        WEB_INDICATOR_DEFAULT_HEIGHT = FastWebUtils.dp2px(context, 3);
    }

    public void setColor(int color) {
        this.mColor = color;
        mPaint.setColor(color);
    }

    public void setColor(String color) {
        this.setColor(Color.parseColor(color));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int wMode = MeasureSpec.getMode(widthMeasureSpec);
        int w = MeasureSpec.getSize(widthMeasureSpec);
        int hMode = MeasureSpec.getMode(heightMeasureSpec);
        int h = MeasureSpec.getSize(heightMeasureSpec);

        if (wMode == MeasureSpec.AT_MOST) {
            w = w <= getContext().getResources().getDisplayMetrics().widthPixels ? w : getContext().getResources().getDisplayMetrics().widthPixels;
        }
        if (hMode == MeasureSpec.AT_MOST) {
            h = WEB_INDICATOR_DEFAULT_HEIGHT;
        }
        this.setMeasuredDimension(w, h);
    }

    @Override
    protected void onDraw(Canvas canvas) {
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        canvas.drawRect(0, 0, mCurrentProgress / 100 * Float.valueOf(this.getWidth()), this.getHeight(), mPaint);
    }

    @Override
    public void show() {
        if (getVisibility() == View.GONE) {
            this.setVisibility(VISIBLE);
            mCurrentProgress = 0f;
            startAnim(false);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.mTargetWidth = getMeasuredWidth();
        int screenWidth = getContext().getResources().getDisplayMetrics().widthPixels;
        if (mTargetWidth >= screenWidth) {
            CURRENT_MAX_DECELERATE_SPEED_DURATION = MAX_DECELERATE_SPEED_DURATION;
            CURRENT_MAX_UNIFORM_SPEED_DURATION = MAX_UNIFORM_SPEED_DURATION;
        } else {
            //取比值
            float rate = this.mTargetWidth / Float.valueOf(screenWidth);
            CURRENT_MAX_UNIFORM_SPEED_DURATION = (int) (MAX_UNIFORM_SPEED_DURATION * rate);
            CURRENT_MAX_DECELERATE_SPEED_DURATION = (int) (MAX_DECELERATE_SPEED_DURATION * rate);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mAnimator != null && mAnimator.isStarted()) {
            mAnimator.cancel();
            mAnimator = null;
        }
    }

    public void setProgress(float progress) {
        if (getVisibility() == View.GONE) {
            setVisibility(VISIBLE);
        }
        if (progress < 95f) {
            return;
        }
        if (TAG != FINISH) {
            startAnim(true);
        }
    }

    @Override
    public void hide() {
        TAG = FINISH;
    }

    @Override
    public void reset() {
        mCurrentProgress = 0;
        if (mAnimator != null && mAnimator.isStarted()) {
            mAnimator.cancel();
        }
    }

    @Override
    public void setProgress(int newProgress) {
        setProgress(Float.valueOf(newProgress));
    }

    private void startAnim(boolean isFinished) {
        float v = isFinished ? 100 : 95;
        if (mAnimator != null && mAnimator.isStarted()) {
            mAnimator.cancel();
        }
        mCurrentProgress = mCurrentProgress == 0f ? 0.00000001f : mCurrentProgress;
        if (!isFinished) {
            ValueAnimator animator = ValueAnimator.ofFloat(mCurrentProgress, v);
            float residue = 1f - mCurrentProgress / 100 - 0.05f;
            animator.setInterpolator(new LinearInterpolator());
            animator.setDuration((long) (residue * CURRENT_MAX_UNIFORM_SPEED_DURATION));
            animator.addUpdateListener(mAnimatorUpdateListener);
            animator.start();
            this.mAnimator = animator;
        } else {
            ValueAnimator segment = null;
            if (mCurrentProgress < 95f) {
                segment = ValueAnimator.ofFloat(mCurrentProgress, 95);
                float residue = 1f - mCurrentProgress / 100f - 0.05f;
                segment.setInterpolator(new LinearInterpolator());
                segment.setDuration((long) (residue * CURRENT_MAX_DECELERATE_SPEED_DURATION));
                segment.setInterpolator(new DecelerateInterpolator());
                segment.addUpdateListener(mAnimatorUpdateListener);
            }
            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this, "alpha", 1f, 0f);
            objectAnimator.setDuration(DO_END_ANIMATION_DURATION);
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(95f, 100f);
            valueAnimator.setDuration(DO_END_ANIMATION_DURATION);
            valueAnimator.addUpdateListener(mAnimatorUpdateListener);
            AnimatorSet mAnimatorSet = new AnimatorSet();
            mAnimatorSet.playTogether(objectAnimator, valueAnimator);
            if (segment != null) {
                AnimatorSet set = new AnimatorSet();
                set.play(mAnimatorSet).after(segment);
                mAnimatorSet = set;
            }
            mAnimatorSet.addListener(mAnimatorListenerAdapter);
            mAnimatorSet.start();
            mAnimator = mAnimatorSet;
        }
        TAG = STARTED;
        mTarger = v;
    }

    private void doEnd() {
        if (TAG == FINISH && mCurrentProgress == 100f) {
            setVisibility(GONE);
            mCurrentProgress = 0f;
            this.setAlpha(1f);
        }
    }


    @Override
    public LayoutParams offerLayoutParams() {
        return new LayoutParams(-1,WEB_INDICATOR_DEFAULT_HEIGHT);
    }
}
