package com.artcool.fastweb.core.web;

/**
 *
 * @author wuyibin
 * @date 2019/5/21
 */
public interface WebLifeCycle {

    void onResume();

    void onPause();

    void onDestroy();
}
