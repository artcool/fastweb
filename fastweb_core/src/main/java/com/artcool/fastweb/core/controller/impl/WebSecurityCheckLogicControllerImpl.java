package com.artcool.fastweb.core.controller.impl;

import android.os.Build;
import android.webkit.WebView;

import com.artcool.fastweb.core.config.FastWebConfig;
import com.artcool.fastweb.core.controller.WebSecurityCheckLogicController;
import com.artcool.fastweb.core.security.SecurityType;

import java.util.LinkedHashMap;

/**
 * @author wuyibin
 * @date 2019/5/21
 */
public class WebSecurityCheckLogicControllerImpl implements WebSecurityCheckLogicController {

    public static WebSecurityCheckLogicControllerImpl getInstance() {
        return new WebSecurityCheckLogicControllerImpl();
    }

    @Override
    public void dealHoneyComb(WebView webView) {
        if (Build.VERSION_CODES.HONEYCOMB > Build.VERSION.SDK_INT || Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return;
        }
        webView.removeJavascriptInterface("searchBoxJavaBridge_");
        webView.removeJavascriptInterface("accessibility");
        webView.removeJavascriptInterface("accessibilityTraversal");
    }

    @Override
    public void dealJsInterface(LinkedHashMap<String, Object> objects, SecurityType securityType) {
        if (securityType == SecurityType.STRICT
                && FastWebConfig.WEBVIEW_TYPE != FastWebConfig.WEBVIEW_FASTWEB_SAFE_TYPE
                && Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            objects.clear();
            objects = null;
            System.gc();
        }
    }
}
