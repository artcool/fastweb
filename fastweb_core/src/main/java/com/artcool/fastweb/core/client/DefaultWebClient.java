package com.artcool.fastweb.core.client;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.webkit.HttpAuthHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.alipay.sdk.app.H5PayCallback;
import com.alipay.sdk.app.PayTask;
import com.alipay.sdk.util.H5PayResultModel;
import com.artcool.fastweb.core.base.BaseMiddlewareWebClient;
import com.artcool.fastweb.core.builder.DefaultWebClientBuilder;
import com.artcool.fastweb.core.controller.AbsFastWebUIController;
import com.artcool.fastweb.core.utils.FastWebUtils;

import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author wuyibin
 * @date 2019/5/6
 */
public class DefaultWebClient extends BaseMiddlewareWebClient {

    /**
     * 跳转协议
     */
    public static final String INTENT_SCHEME = "intent://";
    /**
     * 唤醒微信支付
     */
    public static final String WECHAT_PAY_SCHEME = "weixin://wap/pay?";
    /**
     * 唤醒支付宝支付
     */
    public static final String ALIPAY_SCHEME = "alipays://";
    /**
     * http协议
     */
    public static final String HTTP_SCHEME = "http://";
    /**
     * https协议
     */
    public static final String HTTPS_SCHEME = "https://";
    /**
     * 直接打开其他页面
     */
    public static final int DERECT_OPEN_OTHER_PAGE = 1001;
    /**
     * 询问用户是否前往其他页面
     */
    public static final int ASK_USER_OPEN_OTHER_PAGE = DERECT_OPEN_OTHER_PAGE >> 2;
    /**
     * 不允许打开其他页面
     */
    public static final int DISALLOW_OPEN_OTHER_PAGE = DERECT_OPEN_OTHER_PAGE >> 4;
    /**
     * 发送短信
     */
    public static final String SCHEME_SMS = "sms:";
    /**
     * 缩放
     */
    private static final int CONSTANTS_ABNORMAL_BIG = 7;
    /**
     * 判断用户是否重写了WebViewClient的某一个方法
     */
    private static final String WEBVIEWCLIENT_PATH = "android.webkit.WebViewClient";
    /**
     * true ：应用内依赖了Alipay lib
     * false：没有依赖
     */
    private static final boolean HAS_ALIPAY_LIB;

    static {
        boolean tag = true;
        try {
            Class.forName("com.alipay.sdk.app.PayTask");
        } catch (ClassNotFoundException e) {
            tag = false;
        }
        HAS_ALIPAY_LIB = tag;
    }

    private WeakReference<Activity> mWeakReference = null;
    private WebViewClient mWebViewClient;
    private boolean webViewClientHelper = true;
    /**
     * 默认为询问用户
     */
    private int mUrlHandleWays = ASK_USER_OPEN_OTHER_PAGE;
    /**
     * 是否拦截找不到相应页面的url，默认拦截
     */
    private boolean isInterceptUnkownUrl = true;
    private WeakReference<AbsFastWebUIController> mWebUIController = null;
    private WebView mWebView;
    /**
     * 弹窗回调
     */
    private Handler.Callback mCallback = null;
    /**
     * 展示错误页面
     */
    private Method onShowErrorPage = null;
    /**
     * alipay paytask对象
     */
    private Object mPayTask;
    /**
     * 缓存当前出现的错误页面
     */
    private Set<String> mErrorUrlsSet = new HashSet<>();
    /**
     * 缓存等待加载完的页面
     */
    private Set<String> mWattingFinishSet = new HashSet<>();

    public DefaultWebClient(DefaultWebClientBuilder builder) {
        this.mWebView = builder.getWebView();
        this.mWebViewClient = builder.getWebViewClient();
        mWeakReference = new WeakReference<>(builder.getActivity());
        this.webViewClientHelper = builder.isWebClientHelper();
        mWebUIController = new WeakReference<>(FastWebUtils.getFastWebUIController(builder.getWebView()));
        isInterceptUnkownUrl = builder.isInterceptUnkownScheme();
        if (builder.getUrlHandleWays() <= 0) {
            mUrlHandleWays = ASK_USER_OPEN_OTHER_PAGE;
        } else {
            mUrlHandleWays = builder.getUrlHandleWays();
        }
    }

    public static DefaultWebClientBuilder createBuilder() {
        return new DefaultWebClientBuilder();
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            String url = request.getUrl().toString();
            if (url.startsWith(HTTP_SCHEME) || url.startsWith(HTTPS_SCHEME)) {
                return (webViewClientHelper && HAS_ALIPAY_LIB && isAlipay(view, url));
            }
            if (!webViewClientHelper) {
                return super.shouldOverrideUrlLoading(view, request);
            }
            if (handleCommonLink(url)) {
                return true;
            }
            if (url.startsWith(INTENT_SCHEME)) {
                handleIntentUrl(url);
                return true;
            }
            if (url.startsWith(WECHAT_PAY_SCHEME)) {
                //微信支付
                startToActivity(url);
                return true;
            }
            if (url.startsWith(ALIPAY_SCHEME) && lookup(url)) {
                //支付宝
                return true;
            }
            if (queryActivitiesNumber(url) > 0 && lookup(url)) {
                return true;
            }
            if (isInterceptUnkownUrl) {
                return true;
            }
        }
        return super.shouldOverrideUrlLoading(view, request);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.startsWith(HTTPS_SCHEME) || url.startsWith(HTTP_SCHEME)) {
            return (webViewClientHelper && HAS_ALIPAY_LIB && isAlipay(view, url));
        }
        if (!webViewClientHelper) {
            return false;
        }
        if (handleCommonLink(url)) {
            return true;
        }
        if (url.startsWith(INTENT_SCHEME)) {
            handleIntentUrl(url);
            return true;
        }
        if (url.startsWith(WECHAT_PAY_SCHEME)) {
            startToActivity(url);
            return true;
        }
        if (url.startsWith(ALIPAY_SCHEME) && lookup(url)) {
            return true;
        }
        if (queryActivitiesNumber(url) > 0 && deepLink(url)) {
            return true;
        }
        if (isInterceptUnkownUrl) {
            //没有匹配到任何链接
            return true;
        }
        return super.shouldOverrideUrlLoading(view, url);
    }

    private boolean deepLink(String url) {
        switch (mUrlHandleWays) {
            case DERECT_OPEN_OTHER_PAGE:
                //直接打开APP
                lookup(url);
                break;
            case ASK_USER_OPEN_OTHER_PAGE:
                if (mWebUIController.get() != null) {
                    mWebUIController.get().onOpenOtherPage(this.mWebView, mWebView.getUrl(), getCallBack(url));
                }
                break;
            default:
                break;
        }
        return false;
    }

    private Handler.Callback getCallBack(final String url) {
        if (this.mCallback != null) {
            return this.mCallback;
        }
        return this.mCallback = new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        lookup(url);
                        break;
                    default:
                        return true;
                }
                return true;
            }
        };
    }

    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
        return super.shouldInterceptRequest(view, url);
    }

    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
        return super.shouldInterceptRequest(view, request);
    }

    @Override
    public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
        super.onReceivedHttpAuthRequest(view, handler, host, realm);
    }


    private int queryActivitiesNumber(String url) {
        try {
            if (mWeakReference.get() == null) {
                return 0;
            }
            Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
            PackageManager manager = mWeakReference.get().getPackageManager();
            List<ResolveInfo> resolveInfos = manager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            return resolveInfos == null ? 0 : resolveInfos.size();
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return 0;
        }
    }

    private void startToActivity(String url) {
        try {
            if (mWeakReference.get() == null) {
                return;
            }
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            mWeakReference.get().startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void handleIntentUrl(String url) {
        try {
            if (TextUtils.isEmpty(url) || !url.startsWith(INTENT_SCHEME)) {
                return;
            }
            if (lookup(url)) {
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean lookup(String url) {
        try {
            Intent intent;
            Activity activity;
            if ((activity = mWeakReference.get()) == null) {
                return true;
            }
            PackageManager manager = activity.getPackageManager();
            intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
            ResolveInfo info = manager.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
            if (info != null) {
                //跳转到该应用
                activity.startActivity(intent);
                return true;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 处理普通的url
     * 电话、短信、邮箱
     *
     * @param url
     * @return
     */
    private boolean handleCommonLink(String url) {
        if (url.startsWith(WebView.SCHEME_TEL)
                || url.startsWith(SCHEME_SMS)
                || url.startsWith(WebView.SCHEME_MAILTO)
                || url.startsWith(WebView.SCHEME_GEO)) {
            try {
                Activity mActivity;
                if ((mActivity = mWeakReference.get()) == null) {
                    return false;
                }
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                mActivity.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }

    private boolean isAlipay(final WebView view, final String url) {
        try {
            Activity mActivity;
            if ((mActivity = mWeakReference.get()) == null) {
                return false;
            }
            if (mPayTask == null) {
                Class<?> clazz = Class.forName("com.alipay.sdk.app.PayTask");
                Constructor<?> constructor = clazz.getConstructor(Activity.class);
                mPayTask = constructor.newInstance(mActivity);
            }
            final PayTask task = (PayTask) mPayTask;
            boolean isIntercepted = task.payInterceptorWithUrl(url, true, new H5PayCallback() {
                @Override
                public void onPayResult(H5PayResultModel result) {
                    String resultUrl = result.getReturnUrl();
                    if (!TextUtils.isEmpty(resultUrl)) {
                        FastWebUtils.runInUiThread(new Runnable() {
                            @Override
                            public void run() {
                                view.loadUrl(url);
                            }
                        });
                    }

                }
            });
            return isIntercepted;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return false;
    }

}
