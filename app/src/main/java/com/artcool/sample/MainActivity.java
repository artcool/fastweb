package com.artcool.sample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.artcool.fastweb.core.FastWeb;

/**
 * @author wuyb
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout mRlActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initListener();
    }

    private void initListener() {
        mRlActivity.setOnClickListener(this);
    }

    private void initView() {
        mRlActivity = findViewById(R.id.rl_activity);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_activity:
                startActivity(new Intent(MainActivity.this,FastWebActivity.class));
                break;
            default:
                break;
        }
    }
}
