package com.artcool.sample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.artcool.fastweb.core.FastWeb;
import com.artcool.fastweb.core.client.DefaultChromeClient;
import com.artcool.fastweb.core.enums.OpenOtherPageWays;
import com.artcool.fastweb.core.security.SecurityType;

/**
 *
 * @author wuyibin
 * @date 2019/7/15
 */
public class FastWebActivity extends AppCompatActivity {

    private LinearLayout container;
    private FastWeb mFastWeb;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fast_web_activity);
        container = findViewById(R.id.container);
        mFastWeb = FastWeb.with(this)
                .setFastWebParent(container,new LinearLayout.LayoutParams(-1,-1))
                .useDefaultIndicator()
                .setSecurityType(SecurityType.STRICT)
                .setOpenOtherPageWays(OpenOtherPageWays.ASK)
                .interceptUnkownUrl()
                .createFastWeb()
                .ready()
                .go("https://www.baidu.com");

    }
}
